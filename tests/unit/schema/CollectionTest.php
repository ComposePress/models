<?php

/**
 * Class CollectionTest
 */
class CollectionTest extends UnitTest {

	/**
	 * @var \ComposePress\Models\EntityCollection
	 */
	private $collection;

	/**
	 *
	 * @throws \ComposePress\Core\Exception\ComponentInitFailure
	 */
	public function test_add() {
		$this->setup_model_singlesite();
		$this->collection->add( $this->model->get_new() );
		$this->assertEquals( 1, $this->collection->get_count() );
	}

	/**
	 *
	 * @throws \ComposePress\Core\Exception\ComponentInitFailure
	 */
	protected function setup_model_singlesite() {
		parent::setup_model_singlesite();

		$this->collection = $this->plugin->container->create( '\ComposePress\Models\EntityCollection' );
		$this->collection->set_parent( $this->model );
	}

	/**
	 * @throws \ComposePress\Core\Exception\ComponentInitFailure
	 */
	public function test_add_duplicate() {
		$this->setup_model_singlesite();
		$model = $this->model->get_new();
		$this->collection->add( $model );
		$this->collection->add( $model );
		$this->assertFalse( $this->collection->add( $model ) );
	}

	/**
	 *
	 * @throws \ComposePress\Core\Exception\ComponentInitFailure
	 */
	public function test_next_one_item() {
		$this->setup_model_singlesite();
		$this->collection->add( $this->model->get_new() );
		$this->collection->next();
		$this->assertEquals( 1, $this->collection->key() );
		$this->assertFalse( $this->collection->valid() );
	}

	/**
	 * @throws \ComposePress\Core\Exception\ComponentInitFailure
	 */
	public function test_next_many_items() {
		$this->setup_model_singlesite();
		$this->collection->add( $this->model->get_new() );
		$this->collection->add( $this->model->get_new() );
		$this->collection->next();
		$this->assertEquals( 1, $this->collection->key() );
	}

	/**
	 * @throws \ComposePress\Core\Exception\ComponentInitFailure
	 */
	public function test_rewind() {
		$this->setup_model_singlesite();
		$this->collection->add( $this->model->get_new() );
		$this->collection->add( $this->model->get_new() );
		$this->collection->next();
		$this->collection->rewind();
		$this->assertEquals( 0, $this->collection->key() );
	}

	/**
	 * @throws \ComposePress\Core\Exception\ComponentInitFailure
	 */
	public function test_remove() {
		$this->setup_model_singlesite();
		$entity = $this->model->get_new();
		$this->collection->add( $entity );
		$this->assertTrue( $this->collection->remove( $entity ) );
	}

	/**
	 * @throws \ComposePress\Core\Exception\ComponentInitFailure
	 */
	public function test_remove_with_many_items_position_start() {
		$this->setup_model_singlesite();
		$entity  = $this->model->get_new();
		$entity2 = $this->model->get_new();
		$entity3 = $this->model->get_new();
		$this->collection->add( $entity );
		$this->collection->add( $entity2 );
		$this->collection->add( $entity3 );

		$this->assertTrue( $this->collection->remove( $entity3 ) );
		$this->assertEquals( 2, $this->collection->get_count() );
		$this->assertEquals( 0, $this->collection->key() );
	}

	/**
	 * @throws \ComposePress\Core\Exception\ComponentInitFailure
	 */
	public function test_remove_with_many_items_position_end() {
		$this->setup_model_singlesite();
		$entity  = $this->model->get_new();
		$entity2 = $this->model->get_new();
		$entity3 = $this->model->get_new();
		$this->collection->add( $entity );
		$this->collection->add( $entity2 );
		$this->collection->add( $entity3 );
		$this->collection->next();
		$this->collection->next();
		$this->collection->next();

		$this->assertTrue( $this->collection->remove( $entity3 ) );
		$this->assertEquals( 2, $this->collection->get_count() );
		$this->assertEquals( 2, $this->collection->key() );
		$this->assertFalse( $this->collection->valid() );
	}

	/**
	 * @throws \ComposePress\Core\Exception\ComponentInitFailure
	 */
	public function test_many_remove_with_many_items_position_start() {
		$this->setup_model_singlesite();
		$entity  = $this->model->get_new();
		$entity2 = $this->model->get_new();
		$entity3 = $this->model->get_new();
		$this->collection->add( $entity );
		$this->collection->add( $entity2 );
		$this->collection->add( $entity3 );

		$this->assertTrue( $this->collection->remove( $entity3 ) );
		$this->assertEquals( 2, $this->collection->get_count() );
		$this->assertEquals( 0, $this->collection->key() );
		$this->assertEquals( $entity2, $this->collection->current() );

		$this->assertTrue( $this->collection->remove( $entity2 ) );
		$this->assertEquals( 1, $this->collection->get_count() );
		$this->assertEquals( 0, $this->collection->key() );
		$this->assertEquals( $entity, $this->collection->current() );

		$this->assertTrue( $this->collection->remove( $entity ) );
		$this->assertEquals( 0, $this->collection->get_count() );
		$this->assertEquals( 0, $this->collection->key() );
	}

	/**
	 * @throws \ComposePress\Core\Exception\ComponentInitFailure
	 */
	public function test_many_remove_with_many_items_position_end() {
		$this->setup_model_singlesite();
		$entity  = $this->model->get_new();
		$entity2 = $this->model->get_new();
		$entity3 = $this->model->get_new();
		$this->collection->add( $entity );
		$this->collection->add( $entity2 );
		$this->collection->add( $entity3 );
		$this->collection->next();
		$this->collection->next();
		$this->collection->next();

		$this->assertTrue( $this->collection->remove( $entity3 ) );
		$this->assertEquals( 2, $this->collection->get_count() );
		$this->assertEquals( 2, $this->collection->key() );
		$this->assertFalse( $this->collection->valid() );

		$this->assertTrue( $this->collection->remove( $entity2 ) );
		$this->assertEquals( 1, $this->collection->get_count() );
		$this->assertEquals( 1, $this->collection->key() );
		$this->assertFalse( $this->collection->valid() );

		$this->assertTrue( $this->collection->remove( $entity ) );
		$this->assertEquals( 0, $this->collection->get_count() );
		$this->assertEquals( 0, $this->collection->key() );
	}

	/**
	 * @throws \ComposePress\Core\Exception\ComponentInitFailure
	 */
	public function test_remove_with_many_items_position_middle() {
		$this->setup_model_singlesite();
		$entity  = $this->model->get_new();
		$entity2 = $this->model->get_new();
		$entity3 = $this->model->get_new();
		$entity4 = $this->model->get_new();
		$entity5 = $this->model->get_new();
		$this->collection->add( $entity );
		$this->collection->add( $entity2 );
		$this->collection->add( $entity3 );
		$this->collection->add( $entity4 );
		$this->collection->add( $entity5 );
		$this->collection->next();
		$this->collection->next();

		$this->assertTrue( $this->collection->remove( $entity5 ) );
		$this->assertEquals( 4, $this->collection->get_count() );
		$this->assertEquals( 2, $this->collection->key() );
		$this->assertEquals( $entity4, $this->collection->current() );
	}

	/**
	 * @throws \ComposePress\Core\Exception\ComponentInitFailure
	 */
	public function test_remove_many_out_of_order_with_many_items_position_middle() {
		$this->setup_model_singlesite();
		$entity  = $this->model->get_new();
		$entity2 = $this->model->get_new();
		$entity3 = $this->model->get_new();
		$entity4 = $this->model->get_new();
		$entity5 = $this->model->get_new();
		$this->collection->add( $entity );
		$this->collection->add( $entity2 );
		$this->collection->add( $entity3 );
		$this->collection->add( $entity4 );
		$this->collection->add( $entity5 );
		$this->collection->next();
		$this->collection->next();

		$this->assertEquals( 2, $this->collection->key() );

		$this->assertTrue( $this->collection->remove( $entity5 ) );
		$this->assertEquals( 4, $this->collection->get_count() );
		$this->assertEquals( 2, $this->collection->key() );
		$this->assertEquals( $entity4, $this->collection->current() );

		$this->assertTrue( $this->collection->remove( $entity ) );
		$this->assertEquals( 3, $this->collection->get_count() );
		$this->assertEquals( 1, $this->collection->key() );
		$this->assertEquals( $entity4, $this->collection->current() );

		$this->assertTrue( $this->collection->remove( $entity4 ) );
		$this->assertEquals( 2, $this->collection->get_count() );
		$this->assertEquals( 1, $this->collection->key() );
		$this->assertEquals( $entity4, $this->collection->current() );

		$this->assertTrue( $this->collection->remove( $entity2 ) );
		$this->assertEquals( 1, $this->collection->get_count() );
		$this->assertEquals( 0, $this->collection->key() );
		$this->assertEquals( $entity3, $this->collection->current() );
	}

	/**
	 * @throws \ComposePress\Core\Exception\ComponentInitFailure
	 * @throws \ComposePress\Models\Exceptions\InvalidData
	 */
	public function test_save() {
		$this->setup_model_singlesite();
		$this->model->table->build( false );
		for ( $i = 0; $i < 5; $i ++ ) {
			$entity = $this->model->get_new();
			$entity->set_data( [ 'first_name' => 'John ' . $i, 'last_name' => 'Doe ' . $i ] );
			$this->collection->add( $entity );
		}

		$this->assertEquals( 5, count( array_filter( $this->collection->save() ) ) );
	}

	/**
	 * @throws \ComposePress\Core\Exception\ComponentInitFailure
	 * @throws \ComposePress\Models\Exceptions\InvalidData
	 * @throws \ComposePress\Models\Exceptions\ModelException
	 */
	public function test_update() {
		$this->setup_model_singlesite();
		$this->model->table->build( false );
		for ( $i = 0; $i < 5; $i ++ ) {
			$entity = $this->model->get_new();
			$entity->set_data( [ 'first_name' => 'John ' . $i, 'last_name' => 'Doe ' . $i ] );
			$entity->save();
			$entity->set_data( [ 'first_name' => 'George ' . $i, 'last_name' => 'Foe ' . $i ] );
			$this->collection->add( $entity );
		}

		$this->assertEquals( 5, count( array_filter( $this->collection->update() ) ) );
	}

	/**
	 * @throws \ComposePress\Core\Exception\ComponentInitFailure
	 * @throws \ComposePress\Models\Exceptions\InvalidData
	 * @throws \ComposePress\Models\Exceptions\ModelException
	 */
	public function test_delete() {
		$this->setup_model_singlesite();
		$this->model->table->build( false );
		for ( $i = 0; $i < 5; $i ++ ) {
			$entity = $this->model->get_new();
			$entity->set_data( [ 'first_name' => 'John ' . $i, 'last_name' => 'Doe ' . $i ] );
			$entity->save();
			$this->collection->add( $entity );
		}

		$this->assertEquals( 5, count( array_filter( $this->collection->delete() ) ) );
	}

	/**
	 * @throws \ComposePress\Core\Exception\ComponentInitFailure
	 * @throws \ComposePress\Models\Exceptions\InvalidData
	 */
	public function test_save_fail() {
		$this->setup_model_singlesite();
		$this->model->table->build( false );
		for ( $i = 0; $i < 5; $i ++ ) {
			$entity = $this->model->get_new();
			if ( $i !== 2 ) {
				$entity->set_data( [ 'first_name' => 'John ' . $i, 'last_name' => 'Doe ' . $i ] );
			}
			$this->collection->add( $entity );
		}

		$this->assertEquals( 4, count( array_filter( $this->collection->save() ) ) );
	}

	/**
	 * @throws \ComposePress\Core\Exception\ComponentInitFailure
	 * @throws \ComposePress\Models\Exceptions\InvalidData
	 * @throws \ComposePress\Models\Exceptions\ModelException
	 */
	public function test_update_fail() {
		$this->setup_model_singlesite();
		$this->model->table->build( false );
		for ( $i = 0; $i < 5; $i ++ ) {
			$entity = $this->model->get_new();
			$entity->set_data( [ 'first_name' => 'John ' . $i, 'last_name' => 'Doe ' . $i ] );
			$entity->save();
			if ( $i !== 2 ) {

				$entity->set_data( [ 'first_name' => 'George ' . $i, 'last_name' => 'Foe ' . $i ] );
			}
			$this->collection->add( $entity );
		}

		$this->assertEquals( 4, count( array_filter( $this->collection->save() ) ) );
	}

	/**
	 * @throws \ComposePress\Core\Exception\ComponentInitFailure
	 * @throws \ComposePress\Models\Exceptions\InvalidData
	 * @throws \ComposePress\Models\Exceptions\ModelException
	 */
	public function test_delete_fail() {
		$this->setup_model_singlesite();
		$this->model->table->build( false );
		for ( $i = 0; $i < 5; $i ++ ) {
			$entity = $this->model->get_new();
			$entity->set_data( [ 'first_name' => 'John ' . $i, 'last_name' => 'Doe ' . $i ] );
			$entity->save();
			if ( $i !== 2 ) {
				$entity->delete();
			}
			$this->collection->add( $entity );
		}
		$this->expectException( '\ComposePress\Models\Exceptions\ModelException' );
		$this->collection->delete();
	}

	/**
	 * @throws \ComposePress\Core\Exception\ComponentInitFailure
	 * @throws \ComposePress\Models\Exceptions\InvalidData
	 * @throws \ComposePress\Models\Exceptions\ModelException
	 */
	public function test_valid() {
		$this->setup_model_singlesite();
		$this->model->table->build( false );
		$this->collection->add( $this->model->get_new() );
		$this->assertTrue( $this->collection->valid() );
	}

	/**
	 * @throws \ComposePress\Core\Exception\ComponentInitFailure
	 * @throws \ComposePress\Models\Exceptions\InvalidData
	 * @throws \ComposePress\Models\Exceptions\ModelException
	 */
	public function test_remove_fail() {
		$this->setup_model_singlesite();
		$this->model->table->build( false );
		$entity = $this->model->get_new();
		$this->collection->add( $this->model->get_new() );
		$this->collection->remove( $entity );
		$this->assertFalse( $this->collection->remove( $entity ) );
	}

	/**
	 * @throws \ComposePress\Core\Exception\ComponentInitFailure
	 * @throws \ComposePress\Models\Exceptions\InvalidData
	 * @throws \ComposePress\Models\Exceptions\ModelException
	 */
	public function test_bad_method() {
		$this->setup_model_singlesite();
		$this->model->table->build( false );
		$this->expectException( '\ComposePress\Models\Exceptions\CollectionException' );
		$this->collection->test();
	}
}
