<?php

class TableSchemaTest extends UnitTest {

	/**
	 * @var \ComposePress\Models\Schema\Table
	 */
	private $table;

	/**
	 *
	 */
	public function tearDown() {
		parent::tearDown();

		$this->table = null;
	}

	/**
	 * @throws \ComposePress\Core\Exception\ComponentInitFailure
	 * @throws \ComposePress\Models\Exceptions\InvalidSchema
	 */
	public function test_build() {
		$this->_do_test( [
			'columns' => [
				'id'         => [
					'type'           => 'TINYINT',
					'length'         => 10,
					'auto_increment' => true,
				],
				'first_name' => [
					'type'   => 'VARCHAR',
					'length' => 255,
				],
				'last_name'  => [
					'type'   => 'VARCHAR',
					'length' => 255,
				],
			],
			'keys'    => [
				[
					'primary' => true,
					'columns' => [ 'id' ],
				],
			],

		], "CREATE TABLE wptests_test (
	id TINYINT(10) NOT NULL AUTO_INCREMENT,
	first_name VARCHAR(255) NOT NULL,
	last_name VARCHAR(255) NOT NULL,
	PRIMARY KEY (id)
)
DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;" );
	}

	/**
	 * @throws \ComposePress\Core\Exception\ComponentInitFailure
	 * @throws \ComposePress\Models\Exceptions\InvalidSchema
	 */
	public function test_build_multiple_keys() {
		$this->_do_test( [
			'columns' => [
				'id'         => [
					'type'           => 'TINYINT',
					'length'         => 10,
					'auto_increment' => true,
				],
				'first_name' => [
					'type'   => 'VARCHAR',
					'length' => 255,
				],
				'last_name'  => [
					'type'   => 'VARCHAR',
					'length' => 255,
				],
				'col1'       => [
					'type'   => 'VARCHAR',
					'length' => 255,
				],
				'col2'       => [
					'type'   => 'VARCHAR',
					'length' => 255,
				],
			],
			'keys'    => [
				[
					'primary' => true,
					'columns' => [ 'id' ],
				],
				[ 'columns' => [ 'col1', 'col2' ] ],
			],
		], "CREATE TABLE wptests_test (
	id TINYINT(10) NOT NULL AUTO_INCREMENT,
	first_name VARCHAR(255) NOT NULL,
	last_name VARCHAR(255) NOT NULL,
	col1 VARCHAR(255) NOT NULL,
	col2 VARCHAR(255) NOT NULL,
	PRIMARY KEY (id),
	KEY (col1,col2)
)
DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;" );
	}

	/**
	 * @throws \ComposePress\Core\Exception\ComponentInitFailure
	 * @throws \ComposePress\Models\Exceptions\InvalidSchema
	 */
	public function test_build_duplicate_primary() {
		$this->_do_test( [
			'columns' => [
				'id'         => [
					'type'           => 'TINYINT',
					'length'         => 10,
					'auto_increment' => true,
				],
				'first_name' => [
					'type'   => 'VARCHAR',
					'length' => 255,
				],
				'last_name'  => [
					'type'   => 'VARCHAR',
					'length' => 255,
				],
				'col1'       => [
					'type'   => 'VARCHAR',
					'length' => 255,
				],
			],
			'keys'    => [
				[
					'primary' => true,
					'columns' => [ 'id' ],
				],
				[
					'primary' => true,
					'columns' => [ 'col1' ],
				],
			],
		], "CREATE TABLE wptests_test (
	id TINYINT(10) NOT NULL AUTO_INCREMENT,
	first_name VARCHAR(255) NOT NULL,
	last_name VARCHAR(255) NOT NULL,
	col1 VARCHAR(255) NOT NULL,
	PRIMARY KEY (id),
	PRIMARY KEY (col1)
)
DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;" );
	}

	/**
	 * @throws \ComposePress\Core\Exception\ComponentInitFailure
	 */
	public function test_build_index_named() {
		$this->_do_test( [
			'columns' => [
				'id'         => [
					'type'           => 'TINYINT',
					'length'         => 10,
					'auto_increment' => true,
				],
				'first_name' => [
					'type'   => 'VARCHAR',
					'length' => 255,
				],
				'last_name'  => [
					'type'   => 'VARCHAR',
					'length' => 255,
				],
				'col1'       => [
					'type'   => 'VARCHAR',
					'length' => 255,
				],
			],
			'indexes' => [
				'test' => [
					'columns' => [
						'col1',
					],
				],
			],
		], "CREATE TABLE wptests_test (
	id TINYINT(10) NOT NULL AUTO_INCREMENT,
	first_name VARCHAR(255) NOT NULL,
	last_name VARCHAR(255) NOT NULL,
	col1 VARCHAR(255) NOT NULL,
	INDEX test (col1)
)
DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;" );
	}

	/**
	 * @throws \ComposePress\Core\Exception\ComponentInitFailure
	 */
	public function test_build_indexes() {
		$this->_do_test( [
			'columns' => [
				'id'         => [
					'type'           => 'TINYINT',
					'length'         => 10,
					'auto_increment' => true,
				],
				'first_name' => [
					'type'   => 'VARCHAR',
					'length' => 255,
				],
				'last_name'  => [
					'type'   => 'VARCHAR',
					'length' => 255,
				],
				'col1'       => [
					'type'   => 'VARCHAR',
					'length' => 255,
				],
				'col2'       => [
					'type'   => 'VARCHAR',
					'length' => 255,
				],
			],
			'keys'    => [
				[
					'primary' => true,
					'columns' => [ 'id' ],
				],
			],
			'indexes' => [
				[
					'columns' => [ 'col1', 'col2' ],
				],

			]
			,
		], "CREATE TABLE wptests_test (
	id TINYINT(10) NOT NULL AUTO_INCREMENT,
	first_name VARCHAR(255) NOT NULL,
	last_name VARCHAR(255) NOT NULL,
	col1 VARCHAR(255) NOT NULL,
	col2 VARCHAR(255) NOT NULL,
	PRIMARY KEY (id),
	INDEX (col1,col2)
)
DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;" );
	}

	/**
	 * @param      $schema
	 * @param null $result
	 *
	 * @throws \ComposePress\Core\Exception\ComponentInitFailure
	 * @throws \ComposePress\Core\Exception\Plugin
	 * @throws \ReflectionException
	 */
	private function _do_test( $schema, $result = null ) {
		$this->setup_model_singlesite();
		$model       = $this->model;
		$this->table = $this->model->table->get_schema_builder( $schema )->create_component( '\ComposePress\Models\Schema\Table' );
		$this->table->set_params( [ 'name' => $this->model->table->get_full_name() ] );
		$this->table->set_params( $schema );
		$this->table->init();

		if ( $result ) {
			$this->assertEquals( $result, $this->table->get_sql() );
		}
	}
}
