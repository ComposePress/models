<?php

/**
 * Class ColumnTest
 */
class ColumnTest extends UnitTest {
	/**
	 * @var \ComposePress\Models\Schema\Column
	 */
	private $column;

	/**
	 *
	 */
	public function tearDown() {
		parent::tearDown();
		$this->column = null;
	}

	/**
	 * @throws \ComposePress\Core\Exception\ComponentInitFailure
	 * @throws \ComposePress\Models\Exceptions\InvalidSchema
	 */
	public function test_build() {
		$this->_do_test(
			[
				'name'           => 'id',
				'type'           => 'TINYINT',
				'length'         => 10,
				'auto_increment' => true,
			], 'id TINYINT(10) NOT NULL AUTO_INCREMENT' );
	}


	/**
	 * @throws \ComposePress\Core\Exception\ComponentInitFailure
	 * @throws \ComposePress\Core\Exception\Plugin
	 * @throws \ReflectionException
	 */
	public function test_build_no_length() {
		$this->_do_test(
			[
				'name' => 'data',
				'type' => 'TEXT',
			], 'data TEXT NOT NULL' );
	}


	/**
	 * @throws \ComposePress\Core\Exception\ComponentInitFailure
	 * @throws \ComposePress\Core\Exception\Plugin
	 * @throws \ReflectionException
	 */
	public function test_build_length_array() {
		$this->_do_test(
			[
				'name'   => 'data',
				'type'   => 'VARCHAR',
				'length' => [ 5, 10 ],
			], 'data VARCHAR(5,10) NOT NULL' );
	}


	/**
	 * @throws \ComposePress\Core\Exception\ComponentInitFailure
	 * @throws \ComposePress\Core\Exception\Plugin
	 * @throws \ReflectionException
	 */
	public function test_build_length_assoc_array() {
		$this->_do_test(
			[
				'name'   => 'data',
				'type'   => 'VARCHAR',
				'length' => [ 'digits' => 5, 'decimals' => 10 ],
			], 'data VARCHAR(5,10) NOT NULL' );
	}


	/**
	 * @throws \ComposePress\Core\Exception\ComponentInitFailure
	 * @throws \ComposePress\Core\Exception\Plugin
	 * @throws \ReflectionException
	 */
	public function test_build_length() {
		$this->_do_test(
			[
				'name'   => 'data',
				'type'   => 'VARCHAR',
				'length' => 10,
			], 'data VARCHAR(10) NOT NULL' );
	}


	/**
	 * @throws \ComposePress\Core\Exception\ComponentInitFailure
	 * @throws \ComposePress\Core\Exception\Plugin
	 * @throws \ReflectionException
	 */
	public function test_build_character_set() {
		$this->_do_test(
			[
				'name'          => 'data',
				'type'          => 'VARCHAR',
				'length'        => 10,
				'character_set' => 'utf8',
			], 'data VARCHAR(10) CHARACTER SET utf8 NOT NULL' );
	}


	/**
	 * @throws \ComposePress\Core\Exception\ComponentInitFailure
	 * @throws \ComposePress\Core\Exception\Plugin
	 * @throws \ReflectionException
	 */
	public function test_build_default_null() {
		$this->_do_test(
			[
				'name'    => 'data',
				'type'    => 'VARCHAR',
				'length'  => 10,
				'default' => 'NULL',
			], 'data VARCHAR(10)' );
	}


	/**
	 * @throws \ComposePress\Core\Exception\ComponentInitFailure
	 * @throws \ComposePress\Core\Exception\Plugin
	 * @throws \ReflectionException
	 */
	public function test_build_default_null_lowercase() {
		$this->_do_test(
			[
				'name'    => 'data',
				'type'    => 'VARCHAR',
				'length'  => 10,
				'default' => 'null',
			], 'data VARCHAR(10)' );
	}


	/**
	 * @throws \ComposePress\Core\Exception\ComponentInitFailure
	 * @throws \ComposePress\Core\Exception\Plugin
	 * @throws \ReflectionException
	 */
	public function test_build_default_zero() {
		$this->_do_test(
			[
				'name'    => 'data',
				'type'    => 'VARCHAR',
				'length'  => 10,
				'default' => '0',
			], 'data VARCHAR(10) DEFAULT 0 NOT NULL' );
	}


	/**
	 * @throws \ComposePress\Core\Exception\ComponentInitFailure
	 * @throws \ComposePress\Core\Exception\Plugin
	 * @throws \ReflectionException
	 */
	public function test_build_default_test() {
		$this->_do_test(
			[
				'name'    => 'data',
				'type'    => 'VARCHAR',
				'length'  => 10,
				'default' => 'test',
			], "data VARCHAR(10) DEFAULT 'test' NOT NULL" );
	}


	/**
	 * @throws \ComposePress\Core\Exception\ComponentInitFailure
	 * @throws \ComposePress\Core\Exception\Plugin
	 * @throws \ReflectionException
	 */
	public function test_build_auto_increment() {
		$this->_do_test(
			[
				'name'           => 'id',
				'type'           => 'TINYINT',
				'length'         => 10,
				'auto_increment' => true,
			], 'id TINYINT(10) NOT NULL AUTO_INCREMENT' );
	}


	/**
	 * @throws \ComposePress\Core\Exception\ComponentInitFailure
	 * @throws \ComposePress\Core\Exception\Plugin
	 * @throws \ReflectionException
	 */
	public function test_build_enum() {
		$this->_do_test(
			[
				'name'    => 'item_type',
				'type'    => 'ENUM',
				'options' => [ 'A', 'B', 'C' ],
			], 'item_type ENUM(A,B,C) NOT NULL' );
	}

	/**
	 * @param      $schema
	 * @param null $result
	 *
	 * @throws \ComposePress\Core\Exception\ComponentInitFailure
	 * @throws \ComposePress\Core\Exception\Plugin
	 * @throws \ReflectionException
	 * @throws \ComposePress\Models\Exceptions\InvalidSchema
	 */
	private function _do_test( $schema, $result = null ) {
		$this->setup_model_singlesite();
		$model                   = $this->model;
		$this->model->query_mode = $model::MODE_SINGLESITE;
		$column                  = $this->plugin->create_object( '\ComposePress\Models\Schema\Column' );
		$this->column            = $column;
		$this->column->init();
		$this->column->set_params( $schema );

		if ( $result ) {
			$this->assertEquals( $result, $this->column->get_sql() );
		}
	}
}
