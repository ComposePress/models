<?php

class KeyTest extends UnitTest {


	/**
	 * @var \ComposePress\Models\Schema\Key
	 */
	private $key;


	/**
	 *
	 */
	public function tearDown() {
		parent::tearDown();
		$this->key = null;
	}

	/**
	 * @throws \ComposePress\Core\Exception\ComponentInitFailure
	 * @throws \ComposePress\Models\Exceptions\InvalidSchema
	 */
	public function test_build_primary() {
		$this->_do_test( [ 'primary' => true, 'columns' => [ 'id' ], ], 'PRIMARY KEY (id)' );
	}

	/**
	 * @throws \ComposePress\Core\Exception\ComponentInitFailure
	 */
	public function test_build_unique() {
		$this->_do_test( [ 'name' => 'test', 'unique' => true, 'columns' => [ 'col1' ], ], 'UNIQUE KEY test (col1)' );
	}

	/**
	 * @throws \ComposePress\Core\Exception\ComponentInitFailure
	 */
	public function test_build_key() {
		$this->_do_test( [ 'name' => 'test', 'columns' => [ 'col1' ], ], 'KEY test (col1)' );
	}

	/**
	 * @throws \ComposePress\Core\Exception\ComponentInitFailure
	 */
	public function test_build_key_no_name() {
		$this->_do_test( [ 'columns' => [ 'col1' ], ], 'KEY (col1)' );
	}

	/**
	 * @throws \ComposePress\Core\Exception\ComponentInitFailure
	 */
	public function test_build_key_multiple_columns() {
		$this->_do_test( [ 'columns' => [ 'col1', 'col2' ], ], 'KEY (col1,col2)' );
	}

	public function test_build_invalid_key() {
		$this->_do_test( [ 'columns' => [ 'bad_col' ], ], null, true );
	}

	/**
	 * @param      $schema
	 * @param null $result
	 * @param bool $exception
	 *
	 * @throws \ComposePress\Core\Exception\ComponentInitFailure
	 * @throws \ComposePress\Models\Exceptions\InvalidSchema
	 * @throws \ReflectionException
	 */
	private function _do_test( $schema, $result = null, $exception = false ) {
		$this->setup_model_singlesite();
		$model = $this->model;
		if ( $exception ) {
			$this->expectException( '\ComposePress\Models\Exceptions\InvalidSchema' );
		}
		$index         = $this->plugin->container->create( '\ComposePress\Models\Schema\Key', [ $schema ] );
		$index->parent = $this->model->table->get_schema_builder( $this->model->get_schema( $model::MODE_SINGLESITE ) )->table;
		$this->key     = $index;
		$this->key->set_params( $schema );
		$this->key->init();

		if ( $result ) {
			$this->assertEquals( $result, $this->key->get_sql() );
		}
	}
}
