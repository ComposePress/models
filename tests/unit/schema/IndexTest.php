<?php

/**
 * Class IndexTest
 */
class IndexTest extends UnitTest {
	/**
	 * @var \ComposePress\Models\Schema\Index
	 */
	private $index;


	/**
	 *
	 */
	public function tearDown() {
		parent::tearDown();
		$this->index = null;
	}

	/**
	 * @throws \ComposePress\Core\Exception\ComponentInitFailure
	 */
	public function test_build_index() {
		$this->_do_test( [
			'name'    => 'test',
			'columns' => [ 'col1' ],
		], 'INDEX test (col1)' );
	}

	/**
	 * @throws \ComposePress\Core\Exception\ComponentInitFailure
	 */
	public function test_build_index_no_name() {
		$this->_do_test( [ 'columns' => [ 'col1' ] ], 'INDEX (col1)' );
	}

	/**
	 * @throws \ComposePress\Core\Exception\ComponentInitFailure
	 */
	public function test_build_index_multiple_columns() {
		$this->_do_test( [ 'columns' => [ 'col1', 'col2' ] ], 'INDEX (col1,col2)' );
	}

	/**
	 * @throws \ComposePress\Core\Exception\ComponentInitFailure
	 */
	public function test_build_invalid_index() {
		$this->_do_test( [ 'columns' => [ 'bal_col' ] ], null, true );
	}

	/**
	 * @throws \ComposePress\Core\Exception\ComponentInitFailure
	 */
	public function test_init() {
		$this->_do_test( [ 'columns' => [ 'col1' ] ], null, false, true );
	}

	/**
	 * @param      $schema
	 * @param null $result
	 * @param bool $exception
	 * @param bool $init
	 *
	 * @throws \ComposePress\Core\Exception\ComponentInitFailure
	 * @throws \ComposePress\Models\Exceptions\InvalidSchema
	 * @throws \ReflectionException
	 */
	private function _do_test( $schema, $result = null, $exception = false, $init = false ) {
		$this->setup_model_singlesite();
		$model = $this->model;
		if ( $exception ) {
			$this->expectException( '\ComposePress\Models\Exceptions\InvalidSchema' );
		}
		$index         = $this->plugin->container->create( '\ComposePress\Models\Schema\Index' );
		$index->parent = $this->model->table->get_schema_builder( $this->model->get_schema( $model::MODE_SINGLESITE ) )->table;
		$this->index   = $index;

		$this->index->set_params( $schema );
		$val = $this->index->init();
		if ( $init ) {
			$this->assertTrue( $val );
		}
		if ( $result ) {
			$this->assertEquals( $result, $this->index->get_sql() );
		}
	}
}
