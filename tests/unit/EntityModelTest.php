<?php

/**
 * Class EntityModelTest
 */
class EntityModelTest extends UnitTest {
	/**
	 * @var \ComposePress\Models\EntityModel
	 */
	private $entity_model;



	/**
	 * @throws \ComposePress\Core\Exception\ComponentInitFailure
	 * @throws \ComposePress\Models\Exceptions\ModelMissing
	 */
	public function test_get_new() {
		$this->setup_model_singlesite();
		$this->model->table->build( false );
		$this->assertInstanceOf( '\ComposePress\Models\EntityModel', $this->entity_model->get_new() );
	}

	/**
	 * @throws \ComposePress\Core\Exception\ComponentInitFailure
	 * @throws \ComposePress\Models\Exceptions\ModelMissing
	 */
	public function test_get_new_no_model() {
		$this->setup_model_singlesite();
		$this->expectException( '\ComposePress\Models\Exceptions\ModelMissing' );
		$this->entity_model->set_parent( null );
		$this->entity_model->get_new();
	}

	/**
	 *
	 * @throws \ComposePress\Core\Exception\ComponentInitFailure
	 */
	protected function setup_model_singlesite() {
		parent::setup_model_singlesite();

		$this->entity_model = $this->plugin->container->create( '\ComposePress\Models\EntityModel' );
		$this->entity_model->set_parent( $this->model );

	}

	/**
	 * @throws \ComposePress\Core\Exception\ComponentInitFailure
	 * @throws \ComposePress\Models\Exceptions\ModelMissing
	 */
	public function test_get_name() {
		$this->setup_model_singlesite();
		$this->model->table->build( false );
		$this->assertEquals( 'test', $this->entity_model->get_name() );
	}

	/**
	 * @throws \ComposePress\Core\Exception\ComponentInitFailure
	 * @throws \ComposePress\Models\Exceptions\InvalidData
	 * @throws \ComposePress\Models\Exceptions\ModelMissing
	 */
	public function test_set_data() {
		$this->setup_model_singlesite();
		$this->model->table->build( false );
		$this->entity_model->set_data( [ 'first_name' => 'John', 'last_name' => 'Doe' ] );
		$this->assertEquals( 'test', $this->entity_model->get_name() );
	}

	/**
	 * @throws \ComposePress\Core\Exception\ComponentInitFailure
	 * @throws \ComposePress\Models\Exceptions\InvalidData
	 * @throws \ComposePress\Models\Exceptions\ModelMissing
	 */
	public function test_set_data_not_array() {
		$this->setup_model_singlesite();
		$this->model->table->build( false );
		$this->expectException( '\ComposePress\Models\Exceptions\InvalidData' );
		/** @noinspection PhpParamsInspection */
		$this->entity_model->set_data( 'test' );
	}

	/**
	 * @throws \ComposePress\Core\Exception\ComponentInitFailure
	 * @throws \ComposePress\Models\Exceptions\InvalidData
	 * @throws \ComposePress\Models\Exceptions\ModelMissing
	 */
	public function test_get_set() {
		$this->setup_model_singlesite();
		$this->model->table->build( false );
		$this->entity_model->set( 'first_name', 'John' );
		$this->assertEquals( 'John', $this->entity_model->get( 'first_name' ) );
	}

	/**
	 * @throws \ComposePress\Core\Exception\ComponentInitFailure
	 * @throws \ComposePress\Models\Exceptions\InvalidData
	 * @throws \ComposePress\Models\Exceptions\ModelMissing
	 */
	public function test_set_bad_field() {
		$this->setup_model_singlesite();
		$this->model->table->build( false );

		$this->expectException( '\ComposePress\Models\Exceptions\InvalidData' );
		$this->entity_model->set( 'test', 'John' );
	}

	/**
	 * @throws \ComposePress\Core\Exception\ComponentInitFailure
	 * @throws \ComposePress\Models\Exceptions\InvalidData
	 * @throws \ComposePress\Models\Exceptions\ModelMissing
	 */
	public function test_get_bad_field() {
		$this->setup_model_singlesite();
		$this->model->table->build( false );

		$this->expectException( '\ComposePress\Models\Exceptions\InvalidData' );
		$this->entity_model->get( 'test' );
	}

	/**
	 * @throws \ComposePress\Core\Exception\ComponentInitFailure
	 * @throws \ComposePress\Models\Exceptions\InvalidData
	 */
	public function test_get_set_func() {
		$this->setup_model_singlesite();
		$this->model->table->build( false );
		$this->entity_model->set( 'first_name', 'John' );
		$this->assertEquals( 'John', $this->entity_model->get( 'first_name' ) );
	}

	/**
	 * @throws \ComposePress\Core\Exception\ComponentInitFailure
	 * @throws \ComposePress\Models\Exceptions\InvalidData
	 */
	public function test_get_set_magic() {
		$this->setup_model_singlesite();
		$this->model->table->build( false );
		$this->entity_model->first_name = 'John';
		$this->assertEquals( 'John', $this->entity_model->first_name );
	}

	/**
	 * @throws \ComposePress\Core\Exception\ComponentInitFailure
	 * @throws \ComposePress\Models\Exceptions\InvalidData
	 * @throws \ComposePress\Models\Exceptions\ModelException
	 */
	public function test_insert() {
		$this->setup_model_singlesite();
		$this->model->table->build( false );
		$this->entity_model->set_data( [ 'first_name' => 'John', 'last_name' => 'Doe' ] );
		$this->assertTrue( $this->entity_model->insert() );
	}

	/**
	 * @throws \ComposePress\Core\Exception\ComponentInitFailure
	 * @throws \ComposePress\Models\Exceptions\InvalidData
	 * @throws \ComposePress\Models\Exceptions\ModelException
	 */
	public function test_insert_not_new() {
		$this->setup_model_singlesite();
		$this->model->table->build( false );
		$this->entity_model->set_data( [ 'first_name' => 'John', 'last_name' => 'Doe' ] );
		$this->entity_model->insert();
		$this->expectException( '\ComposePress\Models\Exceptions\ModelException' );
		$this->entity_model->insert();
	}

	/**
	 * @throws \ComposePress\Core\Exception\ComponentInitFailure
	 * @throws \ComposePress\Models\Exceptions\InvalidData
	 * @throws \ComposePress\Models\Exceptions\ModelException
	 */
	public function test_insert_fail() {
		$this->setup_model_singlesite();
		$this->model->table->build( false );

		$this->entity_model->set_data( [ 'first_name' => 'John', 'last_name' => 'Doe' ] );
		$this->entity_model->insert();

		$entity = $this->entity_model->get_new();
		$entity->set_data( [ 'id' => $this->entity_model->id, 'first_name' => 'John', 'last_name' => 'Doe' ] );

		@ini_set( 'display_errors', 0 );
		$this->plugin->wpdb->hide_errors();

		$this->assertFalse( $entity->insert() );

		@ini_set( 'display_errors', 1 );
		$this->plugin->wpdb->show_errors();

	}

	/**
	 * @throws \ComposePress\Core\Exception\ComponentInitFailure
	 * @throws \ComposePress\Models\Exceptions\ModelException
	 */
	public function test_save_empty() {
		$this->setup_model_singlesite();
		$this->model->table->build( false );
		$this->assertFalse( $this->entity_model->insert() );
	}

	/**
	 * @throws \ComposePress\Core\Exception\ComponentInitFailure
	 * @throws \ComposePress\Models\Exceptions\ModelException
	 * @throws \ComposePress\Models\Exceptions\InvalidData
	 */
	public function test_update() {
		$this->setup_model_singlesite();
		$this->model->table->build( false );
		$this->entity_model->set_data( [ 'first_name' => 'John', 'last_name' => 'Doe' ] );
		$this->entity_model->insert();
		$this->entity_model->set_data( [ 'first_name' => 'George', 'last_name' => 'Foe' ] );
		$this->assertTrue( $this->entity_model->update() );
	}

	/**
	 * @throws \ComposePress\Core\Exception\ComponentInitFailure
	 * @throws \ComposePress\Models\Exceptions\ModelException
	 * @throws \ComposePress\Models\Exceptions\InvalidData
	 */
	public function test_update_fail() {
		$this->setup_model_singlesite();
		$this->model->table->build( false );
		$this->entity_model->set_data( [ 'first_name' => 'John', 'last_name' => 'Doe' ] );
		$this->entity_model->insert();

		$entity = $this->entity_model->get_new();
		$entity->set_data( [ 'first_name' => 'John', 'last_name' => 'Doe' ] );

		@ini_set( 'display_errors', 0 );
		$this->plugin->wpdb->hide_errors();

		$entity->insert();

		$entity->set_data( [
			'id'         => $this->entity_model->id,
			'first_name' => 'George',
			'last_name'  => 'Foe',
		] );

		$this->assertFalse( $entity->update() );

		@ini_set( 'display_errors', 1 );
		$this->plugin->wpdb->show_errors();
	}

	/**
	 * @throws \ComposePress\Core\Exception\ComponentInitFailure
	 * @throws \ComposePress\Models\Exceptions\ModelException
	 * @throws \ComposePress\Models\Exceptions\InvalidData
	 */
	public function test_update_not_dirty() {
		$this->setup_model_singlesite();
		$this->model->table->build( false );
		$this->entity_model->set_data( [ 'first_name' => 'John', 'last_name' => 'Doe' ] );
		$this->entity_model->insert();

		$this->assertFalse( $this->entity_model->update() );
	}

	/**
	 * @throws \ComposePress\Core\Exception\ComponentInitFailure
	 * @throws \ComposePress\Models\Exceptions\ModelException
	 * @throws \ComposePress\Models\Exceptions\InvalidData
	 */
	public function test_update_is_new() {
		$this->setup_model_singlesite();
		$this->model->table->build( false );
		$this->entity_model->set_data( [ 'first_name' => 'John', 'last_name' => 'Doe' ] );
		$this->expectException( '\ComposePress\Models\Exceptions\ModelException' );
		$this->entity_model->update();
	}

	/**
	 * @throws \ComposePress\Core\Exception\ComponentInitFailure
	 * @throws \ComposePress\Models\Exceptions\ModelException
	 * @throws \ComposePress\Models\Exceptions\InvalidData
	 */
	public function test_update_is_no_changes() {
		$this->setup_model_singlesite();
		$this->model->table->build( false );
		$this->entity_model->set_data( [ 'first_name' => 'John', 'last_name' => 'Doe' ] );
		$this->entity_model->insert();
		$this->entity_model->set_data( [ 'first_name' => 'John', 'last_name' => 'Doe' ] );
		$this->assertFalse( $this->entity_model->update() );
	}

	/**
	 * @throws \ComposePress\Core\Exception\ComponentInitFailure
	 * @throws \ComposePress\Models\Exceptions\InvalidData
	 * @throws \ComposePress\Models\Exceptions\ModelException
	 */
	public function test_delete() {
		$this->setup_model_singlesite();
		$this->model->table->build( false );
		$this->entity_model->set_data( [ 'first_name' => 'John', 'last_name' => 'Doe' ] );
		$this->entity_model->insert();
		$this->assertTrue( $this->entity_model->delete() );
	}

	/**
	 * @throws \ComposePress\Core\Exception\ComponentInitFailure
	 * @throws \ComposePress\Models\Exceptions\InvalidData
	 * @throws \ComposePress\Models\Exceptions\ModelException
	 */
	public function test_delete_fail() {
		$this->setup_model_singlesite();
		$this->model->table->build( false );
		$this->entity_model->set_data( [ 'first_name' => 'John', 'last_name' => 'Doe' ] );
		$this->entity_model->insert();
		$this->model->sql( 'DELETE FROM {table}' );
		$this->assertFalse( $this->entity_model->delete() );
	}

	/**
	 * @throws \ComposePress\Core\Exception\ComponentInitFailure
	 * @throws \ComposePress\Models\Exceptions\InvalidData
	 * @throws \ComposePress\Models\Exceptions\ModelException
	 */
	public function test_save_new() {
		$this->setup_model_singlesite();
		$this->model->table->build( false );
		$this->entity_model->set_data( [ 'first_name' => 'John', 'last_name' => 'Doe' ] );
		$this->assertTrue( $this->entity_model->save() );
	}

	/**
	 * @throws \ComposePress\Core\Exception\ComponentInitFailure
	 * @throws \ComposePress\Models\Exceptions\InvalidData
	 * @throws \ComposePress\Models\Exceptions\ModelException
	 */
	public function test_save_update() {
		$this->setup_model_singlesite();
		$this->model->table->build( false );
		$this->entity_model->set_data( [ 'first_name' => 'John', 'last_name' => 'Doe' ] );
		$this->entity_model->insert();
		$this->entity_model->set_data( [ 'first_name' => 'George', 'last_name' => 'Foe' ] );
		$this->assertTrue( $this->entity_model->save() );
	}


	/**
	 * @throws \ComposePress\Core\Exception\ComponentInitFailure
	 * @throws \ComposePress\Models\Exceptions\InvalidData
	 */
	public function test_reset() {
		$this->setup_model_singlesite();
		$this->model->table->build( false );
		$this->entity_model->set_data( [ 'first_name' => 'John', 'last_name' => 'Doe' ] );
		$this->entity_model->reset();
		$this->assertEmpty( $this->entity_model->get_data() );
	}

	/**
	 * @throws \ComposePress\Core\Exception\ComponentInitFailure
	 * @throws \ComposePress\Models\Exceptions\InvalidData
	 * @throws \ComposePress\Models\Exceptions\ModelException
	 */
	public function test_revert() {
		$this->setup_model_singlesite();
		$this->model->table->build( false );
		$this->entity_model->set_data( [ 'first_name' => 'John', 'last_name' => 'Doe' ] );
		$this->entity_model->save();
		$id = $this->entity_model->id;
		$this->entity_model->set_data( [ 'first_name' => 'George', 'last_name' => 'Foe' ] );
		$this->entity_model->revert();
		$this->assertEquals( [
			'id'         => $id,
			'first_name' => 'John',
			'last_name'  => 'Doe',
		], $this->entity_model->get_data() );
	}

	/**
	 * @throws \ComposePress\Core\Exception\ComponentInitFailure
	 * @throws \ComposePress\Models\Exceptions\InvalidData
	 * @throws \ComposePress\Models\Exceptions\ModelException
	 */
	public function test_revert_new() {
		$this->setup_model_singlesite();
		$this->model->table->build( false );
		$this->entity_model->set_data( [ 'first_name' => 'John', 'last_name' => 'Doe' ] );
		$this->entity_model->revert();
		$this->assertEmpty( $this->entity_model->get_data() );
	}

	/**
	 * @throws \ComposePress\Core\Exception\ComponentInitFailure
	 * @throws \ComposePress\Models\Exceptions\InvalidData
	 */
	public function test_get_set_is_new() {
		$this->setup_model_singlesite();
		$this->model->table->build( false );
		$this->entity_model->set_is_new( false );
		$this->assertFalse( $this->entity_model->is_new() );
	}

	/**
	 * @throws \ComposePress\Core\Exception\ComponentInitFailure
	 * @throws \ComposePress\Models\Exceptions\InvalidData
	 */
	public function test_set_is_dirty() {
		$this->setup_model_singlesite();
		$this->model->table->build( false );
		$this->entity_model->set_is_dirty( true );
		$this->assertTrue( $this->entity_model->is_dirty() );
	}

	/**
	 * @throws \ComposePress\Core\Exception\ComponentInitFailure
	 * @throws \ComposePress\Models\Exceptions\InvalidData
	 */
	public function test_get_changes() {
		$this->setup_model_singlesite();
		$this->model->table->build( false );
		$this->entity_model->set_data( [ 'first_name' => 'John', 'last_name' => 'Doe' ] );
		$this->assertEquals( [ 'first_name' => 'John', 'last_name' => 'Doe' ], $this->entity_model->get_changes() );
	}
}
