<?php

use ComposePress\Models\Abstracts\Model;

/**
 * Class ModelTest
 */
class ModelTest extends UnitTest {

	/**
	 *
	 * @throws \ComposePress\Core\Exception\ComponentInitFailure
	 */
	public function test_get_schema() {
		$this->setup_model_singlesite();
		$schema = $this->model->get_schema( $this->model->query_mode );
		$this->assertArrayHasKey( 'columns', $schema );
		$this->assertArrayHasKey( 'primary_key', $schema );
	}


	/**
	 * @throws \ComposePress\Core\Exception\ComponentInitFailure
	 */
	public function test_query_mode_multisite_fail() {
		$this->setup_model_singlesite();
		$this->assertFalse( $this->model->set_query_mode( Model::MODE_MULTISITE ) );
	}

	/**
	 * @throws \ComposePress\Core\Exception\ComponentInitFailure
	 */
	public function test_query_mode_multisite_global_fail() {
		$this->setup_model_singlesite();
		$this->assertFalse( $this->model->set_query_mode( Model::MODE_MULTISITE_GLOBAL ) );
	}

	/**
	 * @throws \ComposePress\Core\Exception\ComponentInitFailure
	 */
	public function test_insert() {
		$this->setup_model_singlesite();
		$this->model->table->build( false );
		$result = $this->model->insert( [ 'first_name' => 'John', 'last_name' => 'Doe' ] );
		$this->assertTrue( is_int( $result ) );
		$this->assertGreaterThan( 0, $result );
		$this->assertTrue( is_int( $this->model->wpdb->insert_id ) );
		$this->assertGreaterThan( 0, $this->model->wpdb->insert_id );
	}

	/**
	 * @throws \ComposePress\Core\Exception\ComponentInitFailure
	 */
	public function test_update() {
		$this->setup_model_singlesite();
		$this->model->table->build( false );
		$this->model->insert( [ 'first_name' => 'John', 'last_name' => 'Doe' ] );
		$result = $this->model->update( [
			'first_name' => 'George',
			'last_name'  => 'Foe',
		], [ 'id' => $this->model->wpdb->insert_id ] );
		$this->assertTrue( is_int( $result ) );
		$this->assertGreaterThan( 0, $result );
	}

	/**
	 * @throws \ComposePress\Core\Exception\ComponentInitFailure
	 */
	public function test_delete() {
		$this->setup_model_singlesite();
		$this->model->table->build( false );
		$this->model->insert( [ 'first_name' => 'John', 'last_name' => 'Doe' ] );
		$result = $this->model->delete( [ 'id' => $this->model->wpdb->insert_id ] );
		$this->assertTrue( is_int( $result ) );
		$this->assertGreaterThan( 0, $result );
	}

	/**
	 * @throws \ComposePress\Core\Exception\ComponentInitFailure
	 */
	public function test_find() {
		$this->setup_model_singlesite();
		$this->model->table->build( false );
		$this->model->insert( [ 'first_name' => 'John', 'last_name' => 'Doe' ] );
		$this->assertInstanceOf( '\ComposePress\Models\EntityModel', $this->model->find( $this->model->wpdb->insert_id ) );
	}

	/**
	 * @throws \ComposePress\Core\Exception\ComponentInitFailure
	 * @throws \ComposePress\Models\Exceptions\InvalidData
	 */
	public function test_find_empty() {
		$this->setup_model_singlesite();
		$this->model->table->build( false );
		$this->assertFalse( $this->model->find( 999 ) );
	}


	/**
	 * @throws \ComposePress\Core\Exception\ComponentInitFailure
	 */
	public function test_get_new() {
		$this->setup_model_singlesite();
		$this->model->table->build( false );
		$this->assertInstanceOf( '\ComposePress\Models\EntityModel', $this->model->get_new() );
	}

	/**
	 * @throws \ComposePress\Core\Exception\ComponentInitFailure
	 */
	public function test_get_new_collection() {
		$this->setup_model_singlesite();
		$this->model->table->build( false );
		$this->assertInstanceOf( '\ComposePress\Models\EntityCollection', $this->model->get_new_collection() );
	}


	/**
	 * @throws \ComposePress\Core\Exception\ComponentInitFailure
	 */
	public function test_query() {
		$this->setup_model_singlesite();
		$this->model->table->build( false );
		$this->model->insert( [ 'first_name' => 'John', 'last_name' => 'Doe' ] );
		$this->assertInstanceOf( '\ComposePress\Models\EntityCollection', $this->model->sql( 'SELECT * FROM {table}' ) );
	}

	/**
	 * @throws \ComposePress\Core\Exception\ComponentInitFailure
	 * @throws \ComposePress\Models\Exceptions\InvalidData
	 */
	public function test_multisite_method_not_multisite() {
		$this->setup_model_singlesite();
		$this->expectException( '\ComposePress\Models\Exceptions\ModelException' );
		$this->model->insert_multisite( [] );
	}

	/**
	 * @throws \ComposePress\Core\Exception\ComponentInitFailure
	 * @throws \ComposePress\Models\Exceptions\InvalidData
	 */
	public function test_bad_method() {
		$this->setup_model_singlesite();
		$this->expectException( '\ComposePress\Models\Exceptions\ModelException' );
		$this->model->test();
	}
}
