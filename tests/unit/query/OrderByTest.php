<?php


/**
 * Class OrderByTest
 */
class OrderByTest extends UnitTest {
	/**
	 * @var \ComposePress\Models\Query\Element\OrderBy
	 */
	private $orderby;

	/**
	 * @throws \ComposePress\Core\Exception\ComponentInitFailure
	 * @throws \ComposePress\Core\Exception\Plugin
	 * @throws \ReflectionException
	 */
	public function test_basic() {
		$this->setup_model_singlesite();

		$this->orderby->add( 'field1' );

		$this->assertEquals( 'ORDER BY `field1`', (string) $this->orderby );
	}

	/**
	 * @throws \ComposePress\Core\Exception\ComponentInitFailure
	 * @throws \ComposePress\Core\Exception\Plugin
	 * @throws \ReflectionException
	 */
	protected function setup_model_singlesite() {
		parent::setup_model_singlesite();

		$orderby               = $this->plugin->container->create( '\ComposePress\Models\Query\Element\OrderBy' );
		$this->orderby         = $orderby;
		$this->orderby->parent = $this->model->query();
	}

	/**
	 * @throws \ComposePress\Core\Exception\ComponentInitFailure
	 * @throws \ComposePress\Core\Exception\Plugin
	 * @throws \ReflectionException
	 */
	public function test_order_raw() {
		$this->setup_model_singlesite();

		$this->orderby->add( 'field1', [ 'raw' => true ] );

		$this->assertEquals( 'ORDER BY field1', (string) $this->orderby );
	}

	/**
	 * @throws \ComposePress\Core\Exception\ComponentInitFailure
	 * @throws \ComposePress\Core\Exception\Plugin
	 * @throws \ReflectionException
	 */
	public function test_order_desc() {
		$this->setup_model_singlesite();

		$this->orderby->add( 'field1', [ 'order' => 'DESC' ] );

		$this->assertEquals( 'ORDER BY `field1` DESC', (string) $this->orderby );
	}
}
