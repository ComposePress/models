<?php

class SelectTest extends UnitTest {
	/**
	 * @var \ComposePress\Models\Query\Statement\Select
	 */
	private $select;

	public function test_no_select() {
		$this->setup_model_singlesite();

		$this->assertEquals( 'SELECT', (string) $this->select );
	}

	protected function setup_model_singlesite() {
		parent::setup_model_singlesite();

		$select               = $this->plugin->container->create( '\ComposePress\Models\Query\Statement\Select' );
		$this->select         = $select;
		$this->select->parent = $this->model->query();
	}

	public function test_number_select() {
		$this->setup_model_singlesite();

		$this->select->add( 1, [ 'raw' => true ] );

		$this->assertEquals( 'SELECT 1', (string) $this->select );
	}

	public function test_bool_select() {
		$this->setup_model_singlesite();

		$this->expectException( '\ComposePress\Models\Exceptions\QueryException' );

		$this->select->add( true );
	}

	public function test_string_select() {
		$this->setup_model_singlesite();

		$this->select->add( 'blah' );

		$this->assertEquals( 'SELECT `blah`', (string) $this->select );
	}

	public function test_select_array_element_no_field() {
		$this->setup_model_singlesite();

		$this->select->add( [ 'select' => [ [] ] ] );

		$this->assertEquals( 'SELECT', (string) $this->select );

	}

	public function test_select_array_element_aliased() {
		$this->setup_model_singlesite();
		$this->select->add( 'blah', [ 'alias' => 'dah' ] );
		$this->assertEquals( 'SELECT `blah` AS dah', (string) $this->select );
	}

	public function test_select_array_element_aliased_raw() {
		$this->setup_model_singlesite();

		$this->select->add( 'blah', [ 'alias' => 'dah', 'raw' => true ] );
		$this->assertEquals( 'SELECT blah AS dah', (string) $this->select );
	}

	public function test_select_subselect() {
		$this->setup_model_singlesite();

		$this->select->add( $this->select->root()->new_query()->select( 1, [ 'raw' => true ] ), [
			'query' => true,
			'alias' => 'sub1',
		] );
		$this->assertEquals( 'SELECT ( SELECT 1 FROM wptests_test ) AS sub1', (string) $this->select );
	}

	public function test_select_subselect_multi() {
		$this->setup_model_singlesite();

		$this->select->add( $this->select->root()->new_query()->select( 1, [ 'raw' => true ] ), [
			'query' => true,
			'alias' => 'sub1',
		] );
		$this->select->add( $this->select->root()->new_query()->select( 1, [ 'raw' => true ] ), [
			'query' => true,
			'alias' => 'sub2',
		] );
		$this->assertEquals( 'SELECT ( SELECT 1 FROM wptests_test ) AS sub1, ( SELECT 1 FROM wptests_test ) AS sub2', (string) $this->select );
	}
}
