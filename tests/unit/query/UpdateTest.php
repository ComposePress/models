<?php


/**
 * Class UpdateTest
 */
class UpdateTest extends UnitTest {
	/**
	 * @var \ComposePress\Models\Query\Statement\Delete
	 */
	private $update;

	/**
	 * @throws \ComposePress\Core\Exception\ComponentInitFailure
	 * @throws \ComposePress\Core\Exception\Plugin
	 * @throws \ReflectionException
	 */
	public function test_basic() {
		$this->setup_model_singlesite();

		$this->assertEquals( 'UPDATE', (string) $this->update );
	}

	/**
	 * @throws \ComposePress\Core\Exception\ComponentInitFailure
	 * @throws \ComposePress\Core\Exception\Plugin
	 * @throws \ReflectionException
	 */
	protected function setup_model_singlesite() {
		parent::setup_model_singlesite();

		$update               = $this->plugin->container->create( '\ComposePress\Models\Query\Statement\Update' );
		$this->update         = $update;
		$this->update->parent = $this->model->query();
	}
}
