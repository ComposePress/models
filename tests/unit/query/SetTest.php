<?php


/**
 * Class SetTest
 */
class SetTest extends UnitTest {
	/**
	 * @var \ComposePress\Models\Query\Element\GroupBy
	 */
	private $set;

	/**
	 * @throws \ComposePress\Core\Exception\ComponentInitFailure
	 * @throws \ComposePress\Core\Exception\Plugin
	 * @throws \ReflectionException
	 */
	public function test_basic() {
		$this->setup_model_singlesite();

		$this->set->add( 'field1', [ 'value' => 'a' ] );

		$this->assertEquals( "SET `field1` = 'a'", (string) $this->set );
	}

	/**
	 * @throws \ComposePress\Core\Exception\ComponentInitFailure
	 * @throws \ComposePress\Core\Exception\Plugin
	 * @throws \ReflectionException
	 */
	protected function setup_model_singlesite() {
		parent::setup_model_singlesite();

		$set               = $this->plugin->container->create( '\ComposePress\Models\Query\Element\Set' );
		$this->set         = $set;
		$this->set->parent = $this->model->query();
	}

	/**
	 * @throws \ComposePress\Core\Exception\ComponentInitFailure
	 * @throws \ComposePress\Core\Exception\Plugin
	 * @throws \ReflectionException
	 */
	public function test_raw() {
		$this->setup_model_singlesite();

		$this->set->add( 'field1', [ 'value' => 1, 'raw' => true ] );

		$this->assertEquals( "SET `field1` = 1", (string) $this->set );
	}

}
