<?php


/**
 * Class HavingTest
 */
class HavingTest extends UnitTest {
	/**
	 * @var \ComposePress\Models\Query\Element\Having
	 */
	private $having;

	/**
	 * @throws \ComposePress\Core\Exception\ComponentInitFailure
	 */
	public function test_no_having() {
		$this->setup_model_singlesite();

		$this->assertEquals( 'HAVING ( )', (string) $this->having );
	}

	/**
	 * @throws \ComposePress\Core\Exception\ComponentInitFailure
	 * @throws \ComposePress\Core\Exception\Plugin
	 * @throws \ReflectionException
	 */
	protected function setup_model_singlesite() {
		parent::setup_model_singlesite();

		$having                  = $this->plugin->container->create( '\ComposePress\Models\Query\Element\Having' );
		$this->having            = $having;
		$this->having->parent    = $this->model->query();
		$this->having->condition = $this->having->root()->new_condition();
	}

	/**
	 * @throws \ComposePress\Core\Exception\ComponentInitFailure
	 * @throws \ComposePress\Core\Exception\Plugin
	 * @throws \ComposePress\Models\Exceptions\QueryException
	 * @throws \ReflectionException
	 */
	public function test_simple_having() {
		$this->setup_model_singlesite();

		$this->having->condition->add_field_condition( 'id', '=', 1 );

		$this->assertEquals( "HAVING ( `id` = '1' )", (string) $this->having );
	}


	/**
	 * @throws \ComposePress\Core\Exception\ComponentInitFailure
	 * @throws \ComposePress\Core\Exception\Plugin
	 * @throws \ComposePress\Models\Exceptions\QueryException
	 * @throws \ReflectionException
	 */
	public function test_simple_having_compare_less_than() {
		$this->setup_model_singlesite();

		$this->having->condition->add_field_condition( 'id', '<', 1 );

		$this->assertEquals( "HAVING ( `id` < '1' )", (string) $this->having );
	}

	/**
	 * @throws \ComposePress\Core\Exception\ComponentInitFailure
	 * @throws \ComposePress\Core\Exception\Plugin
	 * @throws \ComposePress\Models\Exceptions\QueryException
	 * @throws \ReflectionException
	 */
	public function test_simple_having_assoc_compare_in_raw() {
		$this->setup_model_singlesite();

		$this->having->condition->add_field_condition( 'id', 'IN', [ 1, 2, 3 ], false, true );

		$this->assertEquals( "HAVING ( `id` IN (1, 2, 3) )", (string) $this->having );
	}

	/**
	 * @throws \ComposePress\Core\Exception\ComponentInitFailure
	 * @throws \ComposePress\Core\Exception\Plugin
	 * @throws \ComposePress\Models\Exceptions\QueryException
	 * @throws \ReflectionException
	 */
	public function test_simple_having_compare_in() {
		$this->setup_model_singlesite();

		$this->having->condition->add_field_condition( 'first_name', 'IN', [ 'john', 'doe', 'boe' ] );

		$this->assertEquals( "HAVING ( `first_name` IN ('john', 'doe', 'boe') )", (string) $this->having );
	}

	/**
	 * @throws \ComposePress\Core\Exception\ComponentInitFailure
	 * @throws \ComposePress\Core\Exception\Plugin
	 * @throws \ComposePress\Models\Exceptions\QueryException
	 * @throws \ReflectionException
	 */
	public function test_simple_having_compare_or() {
		$this->setup_model_singlesite();

		$this->having->condition->type = 'OR';
		$this->having->condition->add_field_condition( 'first_name', '=', 'john' );
		$this->having->condition->add_field_condition( 'last_name', '=', 'doe' );

		$this->assertEquals( "HAVING ( `first_name` = 'john' OR `last_name` = 'doe' )", (string) $this->having );

	}

	/**
	 * @throws \ComposePress\Core\Exception\ComponentInitFailure
	 * @throws \ComposePress\Core\Exception\Plugin
	 * @throws \ComposePress\Models\Exceptions\QueryException
	 * @throws \ReflectionException
	 */
	public function test_simple_having_assoc_compare_like() {
		$this->setup_model_singlesite();

		$this->having->condition->add_field_condition( 'first_name', 'LIKE', 'john' );
		$this->assertEquals( "HAVING ( `first_name` LIKE '{$this->having->wpdb->placeholder_escape()}john{$this->having->wpdb->placeholder_escape()}' )", (string) $this->having );
	}

	/**
	 * @throws \ComposePress\Core\Exception\ComponentInitFailure
	 * @throws \ComposePress\Core\Exception\Plugin
	 * @throws \ComposePress\Models\Exceptions\QueryException
	 * @throws \ReflectionException
	 */
	public function test_simple_having_assoc_compare_like_dont_auto_match() {
		$this->setup_model_singlesite();

		$this->having->condition->add_field_condition( 'first_name', 'LIKE', 'john', false, false, [ 'auto_like' => false ] );

		$this->assertEquals( "HAVING ( `first_name` LIKE 'john' )", (string) $this->having );
	}

	/**
	 * @throws \ComposePress\Core\Exception\ComponentInitFailure
	 * @throws \ComposePress\Core\Exception\Plugin
	 * @throws \ComposePress\Models\Exceptions\QueryException
	 * @throws \ReflectionException
	 */
	public function test_simple_having_assoc_compare_regexp() {
		$this->setup_model_singlesite();


		$this->having->condition->add_field_condition( 'first_name', 'REGEXP', 'j?ohn' );
		$this->assertEquals( "HAVING ( `first_name` REGEXP 'j?ohn' )", (string) $this->having );
	}
}
