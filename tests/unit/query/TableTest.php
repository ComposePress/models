<?php


/**
 * Class SetTest
 */
class TableTest extends UnitTest {
	/**
	 * @var \ComposePress\Models\Query\Element\Table
	 */
	private $table;

	/**
	 * @throws \ComposePress\Core\Exception\ComponentInitFailure
	 * @throws \ComposePress\Core\Exception\Plugin
	 * @throws \ReflectionException
	 */
	public function test_basic() {
		$this->setup_model_singlesite();

		$this->table->add( 'tablea' );

		$this->assertEquals( 'tablea', (string) $this->table );
	}

	/**
	 * @throws \ComposePress\Core\Exception\ComponentInitFailure
	 * @throws \ComposePress\Core\Exception\Plugin
	 * @throws \ReflectionException
	 */
	protected function setup_model_singlesite() {
		parent::setup_model_singlesite();

		$table               = $this->plugin->container->create( '\ComposePress\Models\Query\Element\Table' );
		$this->table         = $table;
		$this->table->parent = $this->model->query();
	}

	/**
	 * @throws \ComposePress\Core\Exception\ComponentInitFailure
	 * @throws \ComposePress\Core\Exception\Plugin
	 * @throws \ReflectionException
	 */
	public function test_alias() {
		$this->setup_model_singlesite();

		$this->table->add( 'tablea', ['alias' => 'ab' ] );

		$this->assertEquals( "tablea AS ab", (string) $this->table );
	}
	public function test_select() {
		$this->setup_model_singlesite();

		$this->table->root()->select();

		$this->table->add( 'tablea', ['alias' => 'ab' ] );

		$this->assertEquals( "FROM tablea AS ab", (string) $this->table );
	}
}
