<?php


/**
 * Class JoinTest
 */
class JoinTest extends UnitTest {
	/**
	 * @var \ComposePress\Models\Query\Element\Limit
	 */
	private $join;


	/**
	 * @throws \ComposePress\Core\Exception\ComponentInitFailure
	 * @throws \ComposePress\Core\Exception\Plugin
	 * @throws \ComposePress\Models\Exceptions\QueryException
	 * @throws \ReflectionException
	 */
	public function test_basic() {
		$this->setup_model_singlesite();

		$condition = $this->join->root()->new_condition();
		$condition->add_join_condition( 'tableb.c', '=', 'tablea.c' );

		$this->join->add( 'tableb', [ 'condition' => $condition, 'alias' => 'tb', 'type' => 'INNER' ] );

		$this->assertEquals( 'INNER JOIN tableb AS tb ON ( `tableb`.`c` = `tableb`.`c` )', (string) $this->join );
	}

	/**
	 * @throws \ComposePress\Core\Exception\ComponentInitFailure
	 * @throws \ComposePress\Core\Exception\Plugin
	 * @throws \ReflectionException
	 */
	protected function setup_model_singlesite() {
		parent::setup_model_singlesite();

		$join               = $this->plugin->container->create( '\ComposePress\Models\Query\Element\Join' );
		$this->join         = $join;
		$this->join->parent = $this->model->query();
	}


	/**
	 * @throws \ComposePress\Core\Exception\ComponentInitFailure
	 * @throws \ComposePress\Core\Exception\Plugin
	 * @throws \ComposePress\Models\Exceptions\QueryException
	 * @throws \ReflectionException
	 */
	public function test_left_join() {
		$this->setup_model_singlesite();

		$condition = $this->join->root()->new_condition();
		$condition->add_join_condition( 'tableb.c', '=', 'tablea.c' );

		$this->join->add( 'tableb', [ 'condition' => $condition, 'alias' => 'tb', 'type' => 'LEFT' ] );

		$this->assertEquals( 'LEFT JOIN tableb AS tb ON ( `tableb`.`c` = `tableb`.`c` )', (string) $this->join );
	}

}
