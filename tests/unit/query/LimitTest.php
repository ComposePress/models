<?php


/**
 * Class LimitTest
 */
class LimitTest extends UnitTest {
	/**
	 * @var \ComposePress\Models\Query\Element\Limit
	 */
	private $limit;

	/**
	 * @throws \ComposePress\Core\Exception\ComponentInitFailure
	 * @throws \ComposePress\Core\Exception\Plugin
	 * @throws \ReflectionException
	 */
	public function test_basic() {
		$this->setup_model_singlesite();

		$this->limit->end = 1;

		$this->assertEquals( 'LIMIT 1', (string) $this->limit );
	}

	/**
	 * @throws \ComposePress\Core\Exception\ComponentInitFailure
	 * @throws \ComposePress\Core\Exception\Plugin
	 * @throws \ReflectionException
	 */
	protected function setup_model_singlesite() {
		parent::setup_model_singlesite();

		$limit               = $this->plugin->container->create( '\ComposePress\Models\Query\Element\Limit' );
		$this->limit         = $limit;
		$this->limit->parent = $this->model->query();
	}

	/**
	 * @throws \ComposePress\Core\Exception\ComponentInitFailure
	 * @throws \ComposePress\Core\Exception\Plugin
	 * @throws \ReflectionException
	 */
	public function test_offset() {
		$this->setup_model_singlesite();

		$this->limit->start = 5;
		$this->limit->end   = 1;

		$this->assertEquals( 'LIMIT 1, 5', (string) $this->limit );
	}

}
