<?php


class WhereTest extends UnitTest {
	/**
	 * @var \ComposePress\Models\Query\Where
	 */
	private $where;

	public function test_no_where() {
		$this->setup_model_singlesite();

		$this->assertEquals( 'WHERE ( )', (string) $this->where );
	}

	protected function setup_model_singlesite() {
		parent::setup_model_singlesite();

		$where                  = $this->plugin->container->create( '\ComposePress\Models\Query\Element\Where' );
		$this->where            = $where;
		$this->where->parent    = $this->model->query();
		$this->where->condition = $this->where->root()->new_condition();
	}

	public function test_simple_where() {
		$this->setup_model_singlesite();

		$this->where->condition->add_field_condition( 'id', '=', 1 );

		$this->assertEquals( "WHERE ( `id` = '1' )", (string) $this->where );
	}


	public function test_simple_where_compare_less_than() {
		$this->setup_model_singlesite();

		$this->where->condition->add_field_condition( 'id', '<', 1 );

		$this->assertEquals( "WHERE ( `id` < '1' )", (string) $this->where );
	}

	public function test_simple_where_assoc_compare_in_raw() {
		$this->setup_model_singlesite();

		$this->where->condition->add_field_condition( 'id', 'IN', [ 1, 2, 3 ], false, true );

		$this->assertEquals( "WHERE ( `id` IN (1, 2, 3) )", (string) $this->where );
	}

	public function test_simple_where_compare_in() {
		$this->setup_model_singlesite();

		$this->where->condition->add_field_condition( 'first_name', 'IN', [ 'john', 'doe', 'boe' ] );

		$this->assertEquals( "WHERE ( `first_name` IN ('john', 'doe', 'boe') )", (string) $this->where );
	}

	public function test_simple_where_compare_or() {
		$this->setup_model_singlesite();

		$this->where->condition->type = 'OR';
		$this->where->condition->add_field_condition( 'first_name', '=', 'john' );
		$this->where->condition->add_field_condition( 'last_name', '=', 'doe' );

		$this->assertEquals( "WHERE ( `first_name` = 'john' OR `last_name` = 'doe' )", (string) $this->where );

	}

	public function test_simple_where_assoc_compare_like() {
		$this->setup_model_singlesite();

		$this->where->condition->add_field_condition( 'first_name', 'LIKE', 'john' );
		$this->assertEquals( "WHERE ( `first_name` LIKE '{$this->where->wpdb->placeholder_escape()}john{$this->where->wpdb->placeholder_escape()}' )", (string) $this->where );
	}

	public function test_simple_where_assoc_compare_like_dont_auto_match() {
		$this->setup_model_singlesite();

		$this->where->condition->add_field_condition( 'first_name', 'LIKE', 'john', false, false, [ 'auto_like' => false ] );

		$this->assertEquals( "WHERE ( `first_name` LIKE 'john' )", (string) $this->where );
	}

	public function test_simple_where_assoc_compare_regexp() {
		$this->setup_model_singlesite();


		$this->where->condition->add_field_condition( 'first_name', 'REGEXP', 'j?ohn' );
		$this->assertEquals( "WHERE ( `first_name` REGEXP 'j?ohn' )", (string) $this->where );
	}
}
