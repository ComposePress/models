<?php


/**
 * Class GroupByTest
 */
class GroupByTest extends UnitTest {
	/**
	 * @var \ComposePress\Models\Query\Element\GroupBy
	 */
	private $groupby;

	/**
	 * @throws \ComposePress\Core\Exception\ComponentInitFailure
	 * @throws \ComposePress\Core\Exception\Plugin
	 * @throws \ReflectionException
	 */
	public function test_basic() {
		$this->setup_model_singlesite();

		$this->groupby->add( 'field1' );

		$this->assertEquals( 'GROUP BY `field1`', (string) $this->groupby );
	}

	/**
	 * @throws \ComposePress\Core\Exception\ComponentInitFailure
	 * @throws \ComposePress\Core\Exception\Plugin
	 * @throws \ReflectionException
	 */
	protected function setup_model_singlesite() {
		parent::setup_model_singlesite();

		$where                 = $this->plugin->container->create( '\ComposePress\Models\Query\Element\GroupBy' );
		$this->groupby         = $where;
		$this->groupby->parent = $this->model->query();
	}

	/**
	 * @throws \ComposePress\Core\Exception\ComponentInitFailure
	 * @throws \ComposePress\Core\Exception\Plugin
	 * @throws \ReflectionException
	 */
	public function test_order_raw() {
		$this->setup_model_singlesite();

		$this->groupby->add( 'field1', [ 'raw' => true ] );

		$this->assertEquals( 'GROUP BY field1', (string) $this->groupby );
	}

}
