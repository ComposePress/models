<?php


/**
 * Class DeleteTest
 */
class DeleteTest extends UnitTest {
	/**
	 * @var \ComposePress\Models\Query\Statement\Delete
	 */
	private $delete;

	/**
	 * @throws \ComposePress\Core\Exception\ComponentInitFailure
	 * @throws \ComposePress\Core\Exception\Plugin
	 * @throws \ReflectionException
	 */
	public function test_basic() {
		$this->setup_model_singlesite();

		$this->assertEquals( 'DELETE', (string) $this->delete );
	}

	/**
	 * @throws \ComposePress\Core\Exception\ComponentInitFailure
	 * @throws \ComposePress\Core\Exception\Plugin
	 * @throws \ReflectionException
	 */
	protected function setup_model_singlesite() {
		parent::setup_model_singlesite();

		$delete               = $this->plugin->container->create( '\ComposePress\Models\Query\Statement\Delete' );
		$this->delete         = $delete;
		$this->delete->parent = $this->model->query();
	}
}
