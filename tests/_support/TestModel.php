<?php


use ComposePress\Models\Abstracts\Model;

class TestModel extends Model {

	const NAME = 'test';

	/**
	 * @param $mode
	 *
	 * @return mixed
	 */
	public function get_schema( $mode ) {
		return [
			'columns'     => [
				'id'         => [
					'type'           => 'TINYINT',
					'length'         => 10,
					'auto_increment' => true,
				],
				'first_name' => [
					'type'   => 'VARCHAR',
					'length' => 255,
				],
				'last_name'  => [
					'type'   => 'VARCHAR',
					'length' => 255,
				],
				'col1'       => [
					'type'   => 'VARCHAR',
					'length' => 255,
				],
				'col2'       => [
					'type'   => 'VARCHAR',
					'length' => 255,
				],
			],
			'primary_key' => 'id',
		];
	}
}
