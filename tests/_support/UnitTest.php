<?php


class UnitTest extends \Codeception\TestCase\WPTestCase {
	/**
	 * @var \Test_Plugin
	 */
	protected $plugin;

	/**
	 * @var \ComposePress\Models\Abstracts\Model
	 */
	protected $model;

	public function setUp() {
		// before
		parent::setUp();

		$this->plugin = test_plugin();
		error_reporting( E_ALL );
	}

	/**
	 *
	 */
	public function tearDown() {
		// your tear down methods here

		// then
		parent::tearDown();
		remove_all_actions( 'wpmu_new_blog' );
		remove_all_actions( 'wpmu_drop_tables' );
		$this->model = null;
	}

	/**
	 * @throws \ComposePress\Core\Exception\ComponentInitFailure
	 */
	protected function setup_model_singlesite() {
		/** @var \TestModel $model */
		$model         = $this->plugin->container->create( '\TestModel' );
		$model->parent = $this->plugin;
		$model->init();
		$this->model             = $model;
		$this->model->query_mode = $model::MODE_SINGLESITE;
	}
}
