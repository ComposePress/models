<?php


namespace ComposePress\Models\Interfaces;


/**
 * Interface SchemaElement
 *
 * @package ComposePress\Models\Interfaces
 */
interface SchemaElement {

	/**
	 * @return string
	 */
	public function get_sql();
}
