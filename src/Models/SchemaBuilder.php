<?php


namespace ComposePress\Models;


use ComposePress\Models\Abstracts\Model;
use ComposePress\Models\Abstracts\SchemaElement;
use ComposePress\Models\Interfaces\SchemaElement as SchemaElementInterface;

/**
 * Class Builder
 *
 * @package ComposePress\Models\Schema
 * @property Model                             $parent
 * @property \ComposePress\Models\Schema\Table $table
 */
class SchemaBuilder extends SchemaElement implements SchemaElementInterface {

	/**
	 * @var string
	 */
	protected $name;

	/**
	 * @var array
	 */
	protected $columns;

	/**
	 * @var array
	 */
	protected $keys;

	/**
	 * @var array
	 */
	protected $indexes;

	/**
	 * @var string
	 */
	protected $charset;

	/**
	 * @var string
	 */
	protected $primary_key;

	/**
	 * @var \ComposePress\Models\Schema\Table
	 */
	protected $table;

	/**
	 * SchemaBuilder constructor.
	 *
	 * @param \ComposePress\Models\Schema\Table $table
	 * @param array                             $params
	 *
	 * @throws \ComposePress\Models\Exceptions\InvalidSchema
	 */
	public function __construct( \ComposePress\Models\Schema\Table $table ) {
		$this->table = $table;
	}

	/**
	 * @return \ComposePress\Models\Schema\Table
	 */
	public function get_table() {
		return $this->table;
	}

	/**
	 * @param array   $schema
	 *
	 * @param  string $mode
	 *
	 * @throws \ComposePress\Models\Exceptions\InvalidSchema
	 */
	public function get_sql() {
		return $this->table->get_sql();
	}

	/**
	 * @return bool
	 * @throws \ComposePress\Models\Exceptions\InvalidSchema
	 */
	public function setup() {

		if ( ! empty( $this->primary_key ) ) {
			$this->keys[] = [ 'primary' => true, 'columns' => (array) $this->primary_key ];
		}

		if ( ! empty( $this->keys ) ) {
			$primary_found = false;
			foreach ( $this->keys as $index => $key ) {
				if ( isset( $key['primary'] ) && $key['primary'] ) {
					if ( $primary_found ) {
						unset( $this->keys [ $index ] );
						continue;
					}

					$primary_found = true;
					continue;
				}
			}
		}


		$this->table->set_params( $this->get_params( 'table', 'primary_key' ) );

		return true;
	}
}
