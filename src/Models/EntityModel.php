<?php


namespace ComposePress\Models;


use ComposePress\Core\Abstracts\Component_0_7_5_0;
use ComposePress\Models\Exceptions\InvalidData;
use ComposePress\Models\Exceptions\ModelException;
use ComposePress\Models\Exceptions\ModelMissing;

/**
 * Class EntityModel
 *
 * @package ComposePress\Models\Abstracts
 *
 * @property \ComposePress\Models\Abstracts\Model $parent
 * @property \wpdb                                $wpdb
 * @property bool                                 $is_new
 * @property bool                                 $is_dirty
 * @property array                                $data
 * @property-read  array                          $changes
 */
class EntityModel extends Component_0_7_5_0 implements \ArrayAccess {

	/**
	 * Is instance a new model
	 *
	 * @var bool
	 */
	protected $is_new = true;

	/**
	 * Has any changes been made
	 *
	 * @var bool
	 */
	protected $is_dirty = false;

	/**
	 * @var array
	 */
	protected $data = [];

	/**
	 * @var array
	 */
	protected $changes = [];

	/**
	 * @var array
	 */
	protected $schema;

	/**
	 * @var null
	 */
	protected $last_query_mode = null;


	/**
	 * Shortcut to return new instance of current EntityModel
	 *
	 * @return \ComposePress\Models\EntityModel
	 * @throws \ComposePress\Models\Exceptions\ModelMissing
	 */
	public function get_new() {
		$this->check_model();

		return $this->get_parent()->get_new();
	}

	/**
	 * Utility function to check if parent is missing and throw an exception
	 *
	 * @throws \ComposePress\Models\Exceptions\ModelMissing
	 */
	protected function check_model() {
		if ( null === $this->get_parent() ) {
			throw new ModelMissing( sprintf( 'Model for %s has not been set.', $this->get_full_class_name() ) );
		}
	}

	/**
	 * @return bool
	 */
	public function revert() {
		$this->changes  = [];
		$this->is_dirty = false;
		if ( empty( $this->data ) ) {
			$this->is_new = true;
		}

		return true;
	}

	/**
	 * Get the name
	 *
	 * @return bool|mixed
	 * @throws \ComposePress\Models\Exceptions\ModelMissing
	 */
	public function get_name() {
		$this->check_model();

		$parent = $this->get_parent();

		return $parent::NAME;
	}

	/**
	 * @return bool
	 */
	public function is_dirty() {
		return $this->is_dirty;
	}

	/**
	 * @param bool $is_dirty
	 */
	public function set_is_dirty( $is_dirty ) {
		$this->is_dirty = $is_dirty;
	}

	/**
	 * @return bool
	 */
	public function is_new() {
		return $this->is_new;
	}

	/**
	 * @param bool $is_new
	 */
	public function set_is_new( $is_new ) {
		$this->is_new = $is_new;
	}

	/**
	 * @return array
	 */
	public function get_data() {
		return $this->data;
	}

	/**
	 * @param array $data
	 *
	 * @return \ComposePress\Models\EntityModel
	 * @throws \ComposePress\Models\Exceptions\InvalidData
	 */
	public function set_data( $data ) {
		if ( ! is_array( $data ) ) {
			throw new InvalidData( sprintf( 'Data must be an array in %s::set_data()', $this->get_full_class_name() ) );
		}

		foreach ( $data as $name => $value ) {
			$this->set( $name, $value );
		}

		return $this;
	}

	/**
	 * @param $name
	 * @param $value
	 *
	 * @return \ComposePress\Models\EntityModel
	 * @throws \ComposePress\Models\Exceptions\InvalidData
	 */
	public function set( $name, $value ) {
		$schema = $this->get_schema();

		if ( ! isset( $schema['columns'][ $name ] ) ) {
			throw new InvalidData( sprintf( 'Field %s does not exist in model %s', $name, $this->get_parent()->get_full_class_name() ) );
		}
		$this->changes[ $name ] = $value;
		$this->is_dirty         = true;

		return $this;
	}

	/**
	 * @return array
	 */
	public function get_schema() {
		/** @var \ComposePress\Models\Abstracts\Model $parent */
		$parent = $this->get_parent();

		if ( null === $this->schema ) {
			$this->last_query_mode = $parent->query_mode;
			$this->schema          = $parent->get_schema( $this->last_query_mode );
		}

		if ( $this->last_query_mode !== $parent->query_mode ) {
			$this->last_query_mode = $parent->query_mode;
			$this->schema          = $parent->get_schema( $this->last_query_mode );
		}

		return $this->schema;
	}

	/**
	 * @return bool
	 * @throws \ComposePress\Models\Exceptions\ModelException
	 */
	public function save() {

		if ( $this->is_new ) {
			return $this->insert();
		}

		return $this->update();
	}

	/**
	 * @throws \ComposePress\Models\Exceptions\ModelException
	 */
	public function insert() {
		if ( ! $this->is_new ) {
			throw new ModelException( sprintf( 'Can not insert new record. Model is not new in %s', $this->get_parent()->get_full_class_name() ) );
		}

		if ( empty( $this->changes ) ) {
			return false;
		}

		$result = $this->get_parent()->insert( $this->changes );
		if ( ! $result ) {
			return false;
		}
		$id     = $this->get_parent()->wpdb->insert_id;
		$schema = $this->get_schema();

		$this->data                           = $this->changes;
		$this->changes                        = [];
		$this->data[ $schema['primary_key'] ] = $id;
		$this->is_new                         = false;
		$this->is_dirty                       = false;

		return true;
	}

	/**
	 * @throws \ComposePress\Models\Exceptions\ModelException
	 */
	public function update() {
		$schema = $this->get_schema();

		if ( $this->is_new || empty( $this->data[ $schema['primary_key'] ] ) ) {
			throw new ModelException( sprintf( 'Can not update record. Model is new in %s', $this->get_parent()->get_full_class_name() ) );
		}

		if ( ! $this->is_dirty ) {
			return false;
		}

		$changes = array_diff_assoc( $this->changes, $this->data );

		if ( empty( $changes ) ) {
			return false;
		}

		$schema = $this->get_schema();

		$result = $this->get_parent()->update( $changes, [ $schema['primary_key'] => $this->data[ $schema['primary_key'] ] ] );

		if ( ! $result ) {
			return false;
		}

		$this->data     = array_merge( $this->data, $changes );
		$this->is_dirty = false;

		return true;
	}

	/**
	 * @throws \ComposePress\Models\Exceptions\ModelException
	 */
	public function delete() {
		$schema = $this->get_schema();

		if ( $this->is_new || empty( $this->data[ $schema['primary_key'] ] ) ) {
			throw new ModelException( sprintf( 'Can not delete record. Model is new in %s', $this->get_parent()->get_full_class_name() ) );
		}


		$result = $this->get_parent()->delete( [ $schema['primary_key'] => $this->data[ $schema['primary_key'] ] ] );

		if ( ! $result ) {
			return false;
		}

		return $this->reset();
	}

	/**
	 * @return bool
	 */
	public function reset() {
		$this->data     = [];
		$this->changes  = [];
		$this->is_new   = true;
		$this->is_dirty = false;

		return true;
	}

	/**
	 * @param $name
	 *
	 * @return bool|mixed
	 * @throws \ComposePress\Models\Exceptions\InvalidData
	 */
	public function __get( $name ) {
		return $this->get( $name );
	}

	/**
	 * @param $name
	 * @param $value
	 *
	 * @throws \ComposePress\Models\Exceptions\InvalidData
	 */
	public function __set( $name, $value ) {
		$this->set( $name, $value );
	}

	/**
	 * @param $name
	 *
	 * @return mixed
	 * @throws \ComposePress\Models\Exceptions\InvalidData
	 */
	public function get( $name ) {
		$schema = $this->get_schema();

		if ( ! isset( $schema['columns'][ $name ] ) ) {
			throw new InvalidData( sprintf( 'Field %s does not exist in model %s', $name, $this->get_parent()->get_full_class_name() ) );
		}

		if ( isset( $this->changes[ $name ] ) ) {
			return $this->changes[ $name ];
		}

		return $this->data[ $name ];
	}

	/**
	 * @return array
	 */
	public function get_changes() {
		return $this->changes;
	}

	/**
	 * Whether a offset exists
	 *
	 * @link  https://php.net/manual/en/arrayaccess.offsetexists.php
	 *
	 * @param mixed $offset <p>
	 *                      An offset to check for.
	 *                      </p>
	 *
	 * @return boolean true on success or false on failure.
	 * </p>
	 * <p>
	 * The return value will be casted to boolean if non-boolean was returned.
	 * @since 5.0.0
	 */
	public function offsetExists( $offset ) {
		try {
			return (bool) $this->get( $offset );
		} catch ( InvalidData $e ) {
			return false;
		}
	}

	/**
	 * Offset to retrieve
	 *
	 * @link  https://php.net/manual/en/arrayaccess.offsetget.php
	 *
	 * @param mixed $offset <p>
	 *                      The offset to retrieve.
	 *                      </p>
	 *
	 * @return mixed Can return all value types.
	 * @since 5.0.0
	 * @throws \ComposePress\Models\Exceptions\InvalidData
	 */
	public function offsetGet( $offset ) {
		return $this->get( $offset );
	}

	/**
	 * Offset to set
	 *
	 * @link  https://php.net/manual/en/arrayaccess.offsetset.php
	 *
	 * @param mixed $offset <p>
	 *                      The offset to assign the value to.
	 *                      </p>
	 * @param mixed $value  <p>
	 *                      The value to set.
	 *                      </p>
	 *
	 * @return void
	 * @since 5.0.0
	 * @throws \ComposePress\Models\Exceptions\InvalidData
	 */
	public function offsetSet( $offset, $value ) {
		$this->set( $offset, $value );
	}

	/**
	 * Offset to unset
	 *
	 * @link  https://php.net/manual/en/arrayaccess.offsetunset.php
	 *
	 * @param mixed $offset <p>
	 *                      The offset to unset.
	 *                      </p>
	 *
	 * @return void
	 * @since 5.0.0
	 * @throws \ComposePress\Models\Exceptions\InvalidData
	 */
	public function offsetUnset( $offset ) {
		$this->set( $offset, null );
	}
}
