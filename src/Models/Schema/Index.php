<?php


namespace ComposePress\Models\Schema;


use ComposePress\Models\Abstracts\SchemaElement;
use ComposePress\Models\Exceptions\InvalidSchema;
use ComposePress\Models\Interfaces\SchemaElement as SchemaElementInterface;

/**
 * Class Index
 *
 * @package ComposePress\Models\Schema
 * @property \ComposePress\Models\Schema\Table $parent
 */
class Index extends SchemaElement implements SchemaElementInterface {
	/**
	 * @var
	 */
	protected $name;
	/**
	 * @var array
	 */
	protected $columns;

	/**
	 * @return string|null
	 */
	public function get_sql() {
		$output = 'INDEX ';

		if ( ! empty( $this->name ) ) {
			$output .= "{$this->name} ";
		}
		$output .= '(' . implode( ',', (array) $this->columns ) . ')';

		return rtrim( $output );
	}

	/**
	 * @return bool
	 * @throws \ComposePress\Models\Exceptions\InvalidSchema
	 */
	public function setup() {
		$column_list  = (array) $this->parent->get_columns();
		$column_names = [];
		foreach ( $column_list as $column ) {
			$column         = (object) $column;
			$column_names[] = $column->name;
		}
		$bad_columns = array_diff( $this->columns, $column_names );
		if ( ! empty( $bad_columns ) ) {
			throw new InvalidSchema( sprintf( 'Column(s) %s does not exist for Key %s in Model %s', implode( ',', $bad_columns ), $this->name, $this->parent->parent->parent->get_full_class_name() ) );
		}

		return true;
	}
}
