<?php


namespace ComposePress\Models\Schema;


use ComposePress\Models\Abstracts\SchemaElement;
use ComposePress\Models\Interfaces\SchemaElement as SchemaElementInterface;

/**
 * Class Column
 *
 * @package ComposePress\Models\Schema
 */
class Column extends SchemaElement implements SchemaElementInterface {

	/**
	 * @var string
	 */
	protected $name;

	/**
	 * @var string
	 */
	protected $type;

	/**
	 * @var int|array
	 */
	protected $length;
	/**
	 * @var array
	 */
	protected $options;
	/**
	 * @var string
	 */
	protected $character_set;
	/**
	 * @var string
	 */
	protected $default;
	/**
	 * @var bool
	 */
	protected $is_null = false;
	/**
	 * @var bool
	 */
	protected $auto_increment = false;


	/**
	 * @return string|null
	 */
	public function get_sql() {
		$output = "{$this->name} {$this->type}";
		if ( ! empty( $this->length ) || ( 'ENUM' === $this->type && ! empty( $this->options ) ) ) {
			$output .= '(';
			if ( ! empty( $this->length ) ) {
				if ( is_array( $this->length ) ) {
					$assoc = array_values( $this->length ) !== $this->length;
					if ( $assoc ) {
						$output .= "{$this->length['digits']},{$this->length['decimals']}";
					}
					if ( ! $assoc ) {
						$output .= implode( ',', $this->length );
					}
				} else {
					$output .= $this->length;
				}
			}

			if ( 'ENUM' === $this->type && ! empty( $this->options ) ) {
				$output .= implode( ',', (array) $this->options );
			}

			$output .= ')';
		}
		$output .= ' ';

		if ( ! empty( $this->character_set ) ) {
			$output .= "CHARACTER SET {$this->character_set} ";
		}

		if ( null !== $this->default ) {
			$numeric = is_numeric( $this->default );
			$output  .= 'DEFAULT ';
			if ( ! $numeric ) {
				$output .= "'";
			}
			$output .= $this->default;
			if ( ! $numeric ) {
				$output .= "'";
			}
			$output .= ' ';
		}
		if ( ! $this->is_null ) {
			$output .= 'NOT NULL ';
		}
		if ( $this->auto_increment ) {
			$output .= 'AUTO_INCREMENT ';
		}
		$output = rtrim( $output );

		return $output;
	}

	/**
	 * @return string
	 */
	public function get_name() {
		return $this->name;
	}

	/**
	 * @return string
	 */
	public function get_type() {
		return $this->type;
	}

	/**
	 * @return mixed
	 */
	public function get_length() {
		return $this->length;
	}

	/**
	 * @return array
	 */
	public function get_options() {
		return $this->options;
	}

	/**
	 * @return string
	 */
	public function get_character_set() {
		return $this->character_set;
	}

	/**
	 * @return string
	 */
	public function get_default() {
		return $this->default;
	}

	/**
	 * @return bool
	 */
	public function _is_null() {
		return $this->is_null;
	}

	/**
	 * @param string $type
	 */
	public function set_type( $type ) {
		$type       = strtoupper( $type );
		$this->type = $type;
	}

	/**
	 * @param string|null $default
	 */
	public function set_default( $default ) {

		if ( 'NULL' === strtoupper( $default ) ) {
			$default       = null;
			$this->is_null = true;
		}
		$this->default = $default;
	}
}
