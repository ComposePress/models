<?php


namespace ComposePress\Models\Schema;


use ComposePress\Models\Abstracts\SchemaElement;
use ComposePress\Models\Exceptions\InvalidSchema;
use ComposePress\Models\Interfaces\SchemaElement as SchemaElementInterface;

/**
 * Class Key
 *
 * @package ComposePress\Models\Schema
 * @property \ComposePress\Models\Schema\Table $parent
 */
class Key extends SchemaElement implements SchemaElementInterface {
	/**
	 * @var
	 */
	protected $name;
	/**
	 * @var bool
	 */
	protected $primary = false;
	/**
	 * @var
	 */
	protected $unique = false;
	/**
	 * @var array
	 */
	protected $columns;

	/**
	 * @return string|null
	 */
	public function get_sql() {
		$output = '';

		if ( $this->primary ) {
			$output .= 'PRIMARY ';
		}
		if ( $this->unique && ! $this->primary ) {
			$output .= 'UNIQUE ';
		}

		$output .= 'KEY ';

		if ( ! $this->primary && ! empty( $this->name ) ) {
			$output .= "{$this->name} ";
		}
		$output .= '(' . implode( ',', (array) $this->columns ) . ')';

		return rtrim( $output );
	}

	/**
	 * @param array $columns
	 */
	public function set_columns( $columns ) {
		$this->columns = $columns;
	}

	/**
	 * @return bool
	 * @throws \ComposePress\Models\Exceptions\InvalidSchema
	 */
	public function setup() {
		$column_list  = $this->parent->get_columns();
		$column_names = [];
		foreach ( $column_list as $column ) {
			$column         = (object) $column;
			$column_names[] = $column->name;
		}
		$bad_columns = array_diff( $this->columns, $column_names );
		if ( ! empty( $bad_columns ) ) {
			throw new InvalidSchema( sprintf( 'Column(s) %s does not exist for Key %s in Model %s', implode( ',', $bad_columns ), $this->name, $this->parent->parent->parent->get_full_class_name() ) );
		}

		return true;
	}

	/**
	 * @return mixed
	 */
	public function get_name() {
		return $this->name;
	}

	/**
	 * @return bool
	 */
	public function is_primary() {
		return $this->primary;
	}

	/**
	 * @return mixed
	 */
	public function get_unique() {
		return $this->unique;
	}

	/**
	 * @return array
	 */
	public function get_columns() {
		return $this->columns;
	}
}
