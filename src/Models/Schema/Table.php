<?php


namespace ComposePress\Models\Schema;


use ComposePress\Models\Abstracts\SchemaElement;
use ComposePress\Models\Interfaces\SchemaElement as SchemaElementInterface;

/**
 * Class Table
 *
 * @package ComposePress\Models\Schema
 * @property \ComposePress\Models\SchemaBuilder $parent
 */
class Table extends SchemaElement implements SchemaElementInterface {

	/**
	 * @var string
	 */
	protected $name;

	/**
	 * @var array
	 */
	protected $columns = [];

	/**
	 * @var array
	 */
	protected $keys = [];

	/**
	 * @var array
	 */
	protected $indexes = [];

	/**
	 * @var string
	 */
	protected $charset;

	/**
	 * @param array $columns
	 */
	public function set_columns( $columns ) {
		$this->set_array_param_of_type( 'columns', $columns, '\ComposePress\Models\Schema\Column' );
	}

	/**
	 * @param $param
	 * @param $items
	 * @param $type
	 *
	 * @throws \ComposePress\Core\Exception\Plugin
	 */
	protected function set_array_param_of_type( $param, $items, $type ) {
		if ( ! is_array( $items ) ) {
			return;
		}

		$new_items = [];
		foreach ( $items as $name => $item ) {
			if ( is_array( $item ) ) {
				if ( ! isset( $item['name'] ) && ! is_numeric( $name ) ) {
					$item['name'] = $name;
				}
				$new_item = $this->create_component( $type );
				$new_item->set_params( $item );
				$new_item->init();
			}
			if ( null !== $new_item->name ) {
				$new_items  [ $new_item->name ] = $new_item;
				continue;
			}
			$new_items [] = $new_item;
		}
		$this->{$param} = $new_items;
	}

	/**
	 * @param array $columns
	 *
	 * @throws \ComposePress\Core\Exception\Plugin
	 */
	public function set_indexes( $indexes ) {
		$this->set_array_param_of_type( 'indexes', $indexes, '\ComposePress\Models\Schema\Index' );
	}

	/**
	 * @param array $keys
	 *
	 * @throws \ComposePress\Core\Exception\Plugin
	 */
	public function set_keys( $keys ) {
		$this->set_array_param_of_type( 'keys', $keys, '\ComposePress\Models\Schema\Key' );
	}

	/**
	 * @return string
	 */
	public function get_sql() {
		$name    = $this->build_name();
		$columns = $this->build_columns();
		$keys    = $this->build_keys();
		$indexes = $this->build_indexes();
		$charset = $this->build_charset();

		/** @noinspection AdditionOperationOnArraysInspection */
		$items = array_merge( $columns, $keys, $indexes );
		foreach ( $items as $index => $item ) {
			$items[ $index ] = "\t" . $items[ $index ];
		}
		$item_output = implode( ",\n", $items );

		return "{$name} (\n{$item_output}\n)\n{$charset};";
	}

	/**
	 * @return string
	 */
	public function get_name() {
		return $this->name;
	}

	/**
	 * @return array
	 */
	public function get_columns() {
		return $this->columns;
	}

	/**
	 * @return array
	 */
	public function get_keys() {
		return $this->keys;
	}

	/**
	 * @return array
	 */
	public function get_indexes() {
		return $this->indexes;
	}

	/**
	 * @return mixed
	 */
	public function get_charset() {
		return $this->charset;
	}

	/**
	 * @param array $keys
	 */

	protected function build_name() {
		return "CREATE TABLE {$this->name}";
	}

	/**
	 * @return array
	 */
	protected function build_columns() {
		return $this->get_element_array_sql( 'columns' );
	}

	/**
	 * @param $param
	 *
	 * @return array
	 */
	protected function get_element_array_sql( $param ) {
		$items = [];
		/** @var SchemaElement $item */
		foreach ( $this->{$param} as $item ) {
			$items[] = $item->get_sql();
		}

		return $items;
	}

	/**
	 * @return array
	 */
	protected function build_keys() {
		return $this->get_element_array_sql( 'keys' );
	}

	/**
	 * @return array
	 */
	protected function build_indexes() {
		return $this->get_element_array_sql( 'indexes' );
	}

	/**
	 * @return mixed
	 */
	protected function build_charset() {
		if ( empty( $this->charset ) ) {
			$this->charset = $this->wpdb->get_charset_collate();
		}

		return $this->charset;
	}
}
