<?php

namespace ComposePress\Models\Exceptions;


/**
 * Class ModelMissing
 *
 * @package ComposePress\Models
 */
class ModelMissing extends \Exception {

}
