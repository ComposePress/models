<?php


namespace ComposePress\Models;


use ComposePress\Core\Abstracts\Component_0_7_5_0;
use ComposePress\Models\Exceptions\CollectionException;

/**
 * Class Collection
 *
 * @package ComposePress\Models
 * @method save()
 * @method update()
 * @method delete()
 * @property \ComposePress\Models\Abstracts\Model $parent
 */
class EntityCollection extends Component_0_7_5_0 implements \Iterator {

	/**
	 * @var int
	 */
	private $position = 0;

	/**
	 * @var \ComposePress\Models\EntityModel[]
	 */
	private $items = [];

	/**
	 * Move forward to next element
	 *
	 * @link  https://php.net/manual/en/iterator.next.php
	 * @return void Any returned value is ignored.
	 * @since 5.0.0
	 */
	public function next() {
		$this->position ++;
	}

	/**
	 * Return the key of the current element
	 *
	 * @link  https://php.net/manual/en/iterator.key.php
	 * @return mixed scalar on success, or null on failure.
	 * @since 5.0.0
	 */
	public function key() {
		return $this->position;
	}

	/**
	 * Return the current element
	 *
	 * @link  https://php.net/manual/en/iterator.current.php
	 * @return mixed Can return any type.
	 * @since 5.0.0
	 */
	public function current() {
		return $this->items[ $this->position ];
	}

	/**
	 * Checks if current position is valid
	 *
	 * @link  https://php.net/manual/en/iterator.valid.php
	 * @return boolean The return value will be casted to boolean and then evaluated.
	 * Returns true on success or false on failure.
	 * @since 5.0.0
	 */
	public function valid() {
		return isset( $this->items[ $this->position ] );
	}

	/**
	 * Rewind the Iterator to the first element
	 *
	 * @link  https://php.net/manual/en/iterator.rewind.php
	 * @return void Any returned value is ignored.
	 * @since 5.0.0
	 */
	public function rewind() {
		$this->position = 0;
	}

	/**
	 * @param \ComposePress\Models\EntityModel $entity_model
	 *
	 * @return bool
	 */
	public function add( EntityModel $entity_model ) {

		foreach ( $this->items as $item ) {
			if ( $entity_model === $item ) {
				return false;
			}
		}

		$this->items[] = $entity_model;

		return true;
	}

	/**
	 * @param \ComposePress\Models\EntityModel $entity_model
	 *
	 * @return bool
	 */
	public function remove( EntityModel $entity_model ) {
		foreach ( $this->items as $position => $item ) {
			if ( $item === $entity_model ) {
				unset( $this->items[ $position ] );
				$this->items = array_values( $this->items );
				if ( $this->position > 0 && ( ( $this->position === $position && $position >= count( $this->items ) - 1 ) || $this->position > $position ) ) {
					$this->position --;
				}

				return true;
			}
		}

		return false;
	}

	/**
	 * @return int
	 */
	public function get_count() {
		return count( $this->items );
	}

	/**
	 *
	 * @throws \ComposePress\Models\Exceptions\ModelException
	 * @throws \ComposePress\Models\Exceptions\CollectionException
	 */
	public function __call( $name, $arguments ) {
		if ( in_array( $name, [ 'save', 'update', 'delete' ] ) ) {
			$results = [];
			foreach ( $this->items as $item ) {
				$results[] = call_user_func_array( [ $item, $name ], $arguments );
			}

			return $results;
		}

		throw new CollectionException( sprintf( 'Method %s does not exist on %s', $name, $this->get_full_class_name() ) );
	}

	/**
	 * @param $items
	 *
	 * @return $this
	 * @throws \ComposePress\Core\Exception\Plugin
	 * @throws \ComposePress\Models\Exceptions\InvalidData
	 */
	public function add_many( $items ) {
		$items = (array) $items;
		foreach ( $items as $item ) {
			$entity = $this->parent->get_new();
			$entity->set_data( $item );

			$this->add( $entity );
		}

		return $this;
	}
}
