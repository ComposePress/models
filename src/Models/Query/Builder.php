<?php


namespace ComposePress\Models\Query;


use ComposePress\Models\Abstracts\QueryElement;
use ComposePress\Models\Exceptions\QueryException;
use ComposePress\Models\Query\Element\Condition;

/**
 * Class Builder
 *
 * @package ComposePress\Models\Query
 * @property \wpdb                                                                   $wpdb
 * @property \ComposePress\Models\Abstracts\Model|\ComposePress\Models\Query\Builder $parent
 */
class Builder extends QueryElement {
	/**
	 * @var \ComposePress\Models\Abstracts\QueryStatement
	 */
	protected $statement;

	/**
	 * @var \ComposePress\Models\Query\Element\Set
	 */
	protected $set;

	/**
	 * @var \ComposePress\Models\Query\Element\Table
	 */
	protected $table;
	/**
	 * @var \ComposePress\Models\Query\Element\Where
	 */
	protected $where;
	/**
	 * @var \ComposePress\Models\Query\Element\Join
	 */
	protected $join;
	/**
	 * @var \ComposePress\Models\Query\Element\GroupBy
	 */
	protected $group_by;

	/**
	 * @var  \ComposePress\Models\Query\Element\Having
	 */
	protected $having;

	/**
	 * @var  \ComposePress\Models\Query\Element\Having
	 */
	protected $order_by;
	/**
	 * @var \ComposePress\Models\Query\Element\Limit
	 */
	protected $limit;


	/**
	 * @var
	 */
	protected $raw;

	public function setup() {
		$this->table( $this->parent->table->full_name );

		return true;
	}


	/**
	 * @param self $query
	 * @param      $alias
	 *
	 * @return \ComposePress\Models\Query\Builder
	 * @throws \ComposePress\Core\Exception\ComponentInitFailure
	 * @throws \ComposePress\Core\Exception\Plugin
	 * @throws \ComposePress\Models\Exceptions\QueryException
	 * @throws \ReflectionException
	 */
	public function select_query( self $query, $alias ) {
		if ( empty( $alias ) ) {
			throw new QueryException( 'Subselect query in SELECT element must have an alias!' );
		}

		return $this->select( $query, [ 'query' => true, 'alias' => $alias ] );
	}

	/**
	 * @param array $column
	 *
	 * @return $this
	 * @throws \ComposePress\Core\Exception\ComponentInitFailure
	 * @throws \ComposePress\Core\Exception\Plugin
	 * @throws \ReflectionException
	 * @throws \ComposePress\Models\Exceptions\QueryException
	 */
	public function select( $column = '*', array $params = [] ) {
		if ( 'SELECT' !== $this->get_mode() ) {
			$this->clear_statement();
		}

		if ( null === $this->statement ) {
			$this->statement = $this->create_component( '\ComposePress\Models\Query\Statement\Select' );
			$this->statement->init();
		}

		if ( '*' === $column && ! isset( $params['raw'] ) ) {
			return $this->select_raw( $column );
		}

		if ( is_array( $column ) ) {
			throw new QueryException( 'SELECT does not support arrays' );
		}

		if ( ! empty( $column ) ) {
			$this->statement->add( $column, $params );
		}

		return $this;
	}

	/**
	 * @param      $column
	 * @param null $alias
	 *
	 * @return \ComposePress\Models\Query\Builder
	 * @throws \ComposePress\Core\Exception\ComponentInitFailure
	 * @throws \ComposePress\Core\Exception\Plugin
	 * @throws \ComposePress\Models\Exceptions\QueryException
	 * @throws \ReflectionException
	 */
	public function select_raw( $column, $alias = null ) {
		$params = [ 'raw' => true ];

		if ( ! empty( $alias ) ) {
			$params['alias'] = $alias;
		}

		return $this->select( $column, $params );
	}

	/**
	 * @param array $table
	 *
	 * @return $this
	 * @throws \ComposePress\Core\Exception\ComponentInitFailure
	 * @throws \ComposePress\Core\Exception\Plugin
	 * @throws \ReflectionException
	 */
	public function table( $table, $alias = null ) {
		if ( null === $this->table ) {
			$this->table = $this->create_component( '\ComposePress\Models\Query\Element\Table' );
			$this->table->init();
		}

		$this->table->add( $table, $alias );

		return $this;
	}

	/**
	 * @return \ComposePress\Models\Query\Builder
	 */
	public function clear_tables() {
		return $this->clear_element( 'table' );
	}

	/**
	 * @param $element
	 *
	 * @return $this
	 */
	protected function clear_element( $element ) {
		if ( $this->{$element} ) {
			if ( $this->{$element}->parent ) {
				$this->{$element}->parent = null;
			}
			$this->{$element} = null;
		}

		return $this;
	}

	/**
	 * @param string $mode
	 *
	 * @throws \ComposePress\Models\Exceptions\QueryException
	 */
	public function where_mode( $mode = 'AND' ) {
		if ( ! ( 'AND' === $mode || 'OR' === $mode ) ) {
			throw new QueryException( 'Only "AND" and "OR" are valid for a WHERE element' );
		}
		$this->where( null );
		$this->where->condition->set_type( $mode );

		return $this;
	}

	/**
	 * @param        $condition
	 * @param string $comparison
	 * @param null   $value
	 * @param bool   $raw_field
	 * @param bool   $raw_value
	 *
	 * @return $this
	 * @throws \ComposePress\Core\Exception\ComponentInitFailure
	 * @throws \ComposePress\Core\Exception\Plugin
	 * @throws \ComposePress\Models\Exceptions\QueryException
	 * @throws \ReflectionException
	 */
	public function where( $condition, $comparison = '=', $value = null, $raw_field = false, $raw_value = false, array $extra_params = [] ) {
		$conditional = null;
		if ( null === $this->where ) {
			$this->where = $this->create_component( '\ComposePress\Models\Query\Element\Where' );
			$this->where->init();
		}

		if ( null === $this->where->condition ) {
			$conditional            = $this->new_condition();
			$this->where->condition = $conditional;
		}

		if ( ! $conditional ) {
			$conditional = $this->where->condition;
		}

		if ( ! empty( $condition ) ) {
			$class = $conditional->get_full_class_name();
			if ( $condition instanceof $class ) {
				$conditional->add_condition( $condition );
			}
			if ( is_string( $condition ) ) {
				$conditional->add_field_condition( $condition, $comparison, $value, $raw_field, $raw_value, $extra_params );
			}
		}

		return $this;
	}

	/**
	 * @return \ComposePress\Models\Query\Element\Condition
	 * @throws \ComposePress\Core\Exception\Plugin
	 */
	public function new_condition() {
		return $this->create_component( '\ComposePress\Models\Query\Element\Condition' );
	}

	/**
	 * @param $sql
	 *
	 * @return $this
	 * @throws \ComposePress\Core\Exception\ComponentInitFailure
	 * @throws \ComposePress\Core\Exception\Plugin
	 * @throws \ComposePress\Models\Exceptions\QueryException
	 * @throws \ReflectionException
	 */
	public function where_raw( $sql ) {
		$this->where( null );
		$this->where->condition->add_raw_condition( $sql );

		return $this;
	}

	/**
	 * @return \ComposePress\Models\Query\Builder
	 */
	public function clear_wheres() {
		return $this->clear_element( 'where' );
	}


	/**
	 * @param        $table
	 * @param        $field
	 * @param        $foreign_field
	 * @param        $condiition
	 * @param string $type
	 *
	 * @return $this
	 * @throws \ComposePress\Core\Exception\ComponentInitFailure
	 * @throws \ComposePress\Core\Exception\Plugin
	 * @throws \ReflectionException
	 */
	public function join( $table, Condition $condition, $alias, $type = 'INNER' ) {
		if ( null === $this->join ) {
			$this->join = $this->create_component( '\ComposePress\Models\Query\Element\Join' );
			$this->join->init();
		}

		$this->join->add( $table, [
			'condition' => $condition,
			'alias'     => $alias,
			'type'      => $type,
		] );

		return $this;
	}

	/**
	 * @return \ComposePress\Models\Query\Builder
	 */
	public function clear_joins() {
		return $this->clear_element( 'join' );
	}

	/**
	 * @param $field
	 * @param $value
	 *
	 * @return $this
	 * @throws \ComposePress\Core\Exception\ComponentInitFailure
	 * @throws \ComposePress\Core\Exception\Plugin
	 * @throws \ReflectionException
	 * @throws \ComposePress\Models\Exceptions\QueryException
	 */
	public function update( $field, $value, $raw = false ) {
		if ( 'UPDATE' !== $this->get_mode() ) {
			$this->clear_statement();
		}

		if ( null === $this->statement ) {
			$this->statement = $this->create_component( '\ComposePress\Models\Query\Statement\Update' );
			$this->statement->init();
		}

		if ( null === $this->set ) {
			$this->statement = $this->create_component( '\ComposePress\Models\Query\Element\Set' );
			$this->set->init();
		}

		if ( ! empty( $field ) ) {
			$this->set->add( $field, [ 'value' => $value, 'raw' => $raw ] );
		}

	}

	/**
	 * @throws \ComposePress\Core\Exception\ComponentInitFailure
	 * @throws \ComposePress\Core\Exception\Plugin
	 * @throws \ReflectionException
	 */
	public function delete() {
		if ( 'DELETE' !== $this->get_mode() ) {
			$this->clear_statement();
		}

		if ( null === $this->statement ) {
			$this->statement = $this->create_component( '\ComposePress\Models\Query\Statement\DELETE' );
			$this->statement->init();
		}
	}

	/**
	 * @return \ComposePress\Models\Query\Builder
	 */
	public function clear_statement() {
		return $this->clear_element( 'statement' )->clear_element( 'set' );
	}


	/**
	 * @param        $field
	 * @param bool   $raw
	 *
	 * @return $this
	 * @throws \ComposePress\Core\Exception\ComponentInitFailure
	 * @throws \ComposePress\Core\Exception\Plugin
	 * @throws \ComposePress\Models\Exceptions\QueryException
	 * @throws \ReflectionException
	 */
	public function group_by( $field, $raw = false ) {
		if ( null === $this->group_by ) {
			$this->group_by = $this->create_component( '\ComposePress\Models\Query\Element\GroupBy' );
			$this->group_by->init();
		}

		$this->group_by->add( $field, [ 'raw' => $raw ] );

		return $this;
	}

	/**
	 * @return \ComposePress\Models\Query\Builder
	 */
	public function clear_group_by() {
		return $this->clear_element( 'group_by' );
	}

	/**
	 * @param        $field
	 * @param string $order
	 * @param bool   $raw
	 *
	 * @return $this
	 * @throws \ComposePress\Core\Exception\ComponentInitFailure
	 * @throws \ComposePress\Core\Exception\Plugin
	 * @throws \ComposePress\Models\Exceptions\QueryException
	 * @throws \ReflectionException
	 */
	public function order_by( $field, $order = 'ASC', $raw = false ) {
		if ( null === $this->group_by ) {
			$this->order_by = $this->create_component( '\ComposePress\Models\Query\Element\OrderBy' );
			$this->order_by->init();
		}

		$this->order_by->add( $field, [ 'order' => $order, 'raw' => $raw ] );

		return $this;
	}

	/**
	 * @return \ComposePress\Models\Query\Builder
	 */
	public function clear_order_by() {
		return $this->clear_element( 'order_by' );
	}

	/**
	 * @param      $end
	 * @param null $start
	 *
	 * @return $this
	 * @throws \ComposePress\Core\Exception\ComponentInitFailure
	 * @throws \ComposePress\Core\Exception\Plugin
	 * @throws \ReflectionException
	 */
	public function limit( $end, $start = null ) {
		if ( null === $this->limit ) {
			$this->limit = $this->create_component( '\ComposePress\Models\Query\Element\Limit' );
			$this->limit->init();
		}

		if ( $start ) {
			$this->limit->start = $start;
		}
		$this->limit->end = $end;

		return $this;
	}

	/**
	 * @return \ComposePress\Models\Query\Builder
	 */
	public function clear_limit() {
		return $this->clear_element( 'limit' );
	}

	/**
	 * @param string $mode
	 *
	 * @return $this
	 * @throws \ComposePress\Core\Exception\ComponentInitFailure
	 * @throws \ComposePress\Core\Exception\Plugin
	 * @throws \ComposePress\Models\Exceptions\QueryException
	 * @throws \ReflectionException
	 */
	public function having_mode( $mode = 'AND' ) {
		if ( ! ( 'AND' === $mode || 'OR' === $mode ) ) {
			throw new QueryException( 'Only "AND" and "OR" are valid for a HAVING element' );
		}
		$this->having( null );
		$this->having->condition->set_type( $mode );

		return $this;
	}

	/**
	 * @param        $condition
	 * @param string $comparison
	 * @param null   $value
	 * @param bool   $raw_field
	 * @param bool   $raw_value
	 * @param array  $extra_params
	 *
	 * @return $this
	 * @throws \ComposePress\Core\Exception\ComponentInitFailure
	 * @throws \ComposePress\Core\Exception\Plugin
	 * @throws \ComposePress\Models\Exceptions\QueryException
	 * @throws \ReflectionException
	 */
	public function having( $condition, $comparison = '=', $value = null, $raw_field = false, $raw_value = false, array $extra_params = [] ) {
		$conditional = null;
		if ( null === $this->where ) {
			$this->having = $this->create_component( '\ComposePress\Models\Query\Element\Having' );
			$this->having->init();
		}

		if ( null === $this->having->condition ) {
			$conditional             = $this->new_condition();
			$this->having->condition = $conditional;
		}

		if ( ! $conditional ) {
			$conditional = $this->where->condition;
		}

		if ( ! empty( $condition ) ) {
			$class = $conditional->get_full_class_name();
			if ( $condition instanceof $class ) {
				$conditional->add_condition( $condition );
			}
			if ( is_string( $condition ) ) {
				$conditional->add_field_condition( $condition, $comparison, $value, $raw_field, $raw_value, $extra_params );
			}
		}

		return $this;
	}

	/**
	 * @return \ComposePress\Models\Query\Builder
	 */
	public function clear_having() {
		return $this->clear_element( 'having' );
	}

	/**
	 * @return bool|string
	 */
	public function get_mode() {
		if ( ! $this->statement ) {
			return false;
		}

		return $this->statement->type;
	}


	/**
	 * @return string
	 * @throws \ComposePress\Models\Exceptions\QueryException
	 */
	public function __toString() {
		$statement = null;
		$table     = null;
		$update    = null;
		$table     = null;
		$join      = null;
		$where     = null;
		$group_by  = null;
		$having    = null;
		$order_by  = null;
		$limit     = null;

		if ( null !== $this->statement ) {
			$statement = (string) $this->statement;
		}
		if ( null !== $this->table ) {
			$table = (string) $this->table;
		}
		if ( null !== $this->join ) {
			$join = (string) $this->join;
		}
		if ( null !== $this->where ) {
			$where = (string) $this->where;
		}
		if ( null !== $this->group_by ) {
			$group_by = (string) $this->group_by;
		}
		if ( null !== $this->having ) {
			$having = (string) $this->having;
		}
		if ( null !== $this->limit ) {
			$limit = (string) $this->limit;
		}

		return trim( "{$statement} ${table} {$join} {$where} {$group_by} {$having} {$order_by} {$limit}" );
	}


	/**
	 * @return array|\ComposePress\Models\EntityCollection|object|null
	 * @throws \ComposePress\Core\Exception\ComponentInitFailure
	 * @throws \ComposePress\Core\Exception\Plugin
	 * @throws \ComposePress\Models\Exceptions\InvalidData
	 * @throws \ReflectionException
	 */
	public function execute() {
		if ( $this->raw ) {
			return $this->execute_raw();
		}

		return $this->parent->get_new_collection()->add_many( $this->execute_raw() );
	}

	/**
	 *
	 */
	protected function execute_raw() {
		return $this->wpdb->get_results( $this->to_sql() );
	}

	/**
	 * @return string
	 * @throws \ComposePress\Models\Exceptions\QueryException
	 */
	public function to_sql() {

		if ( null === $this->statement && ! $this->get_closest( $this->get_full_class_name() ) ) {
			throw new QueryException( 'SELECT element in query can not be empty!' );
		}

		return $this->wpdb->remove_placeholder_escape( (string) $this );
	}

	/**
	 * @param bool $enable
	 *
	 * @return $this
	 */
	public function raw( $enable = true ) {
		$this->raw = $enable;

		return $this;
	}

	/**
	 * @return \ComposePress\Models\Query\Builder
	 * @throws \ComposePress\Core\Exception\ComponentInitFailure
	 * @throws \ComposePress\Core\Exception\Plugin
	 * @throws \ReflectionException
	 */
	public function new_query() {
		return $this->root()->parent->query();
	}

	/**
	 * @param $field
	 *
	 * @return string
	 */
	public function escape( $field ) {
		$parts = explode( '.', $field );
		$parts = array_map( function ( $item ) {
			return "`{$item}`";
		}, $parts );
		$parts = implode( '.', $parts );

		return $parts;
	}
}
