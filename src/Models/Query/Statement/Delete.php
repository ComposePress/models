<?php


namespace ComposePress\Models\Query\Statement;


use ComposePress\Models\Abstracts\QueryStatement;

class Delete extends QueryStatement {
	/**
	 * @var string
	 */
	protected $type = 'DELETE';

	/**
	 * @return string
	 */
	public function __toString() {
		return 'DELETE';
	}
}
