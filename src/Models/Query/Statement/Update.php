<?php


namespace ComposePress\Models\Query\Statement;


use ComposePress\Models\Abstracts\QueryStatement;

class Update extends QueryStatement {
	/**
	 * @var string
	 */
	protected $type = 'UPDATE';
	/**
	 * @return string
	 */
	public function __toString() {
		return 'UPDATE';
	}
}
