<?php


namespace ComposePress\Models\Query\Statement;


use ComposePress\Models\Abstracts\QueryStatement;

class Select extends QueryStatement {
	/**
	 * @var string
	 */
	protected $type = 'SELECT';

	/**
	 * @return string
	 */
	public function __toString() {
		$sql = 'SELECT ';
		foreach ( $this->items as $index => $item ) {
			$query = ! empty( $this->params[ $index ]['query'] );
			$raw   = ! empty( $this->params[ $index ]['raw'] );

			$alias = null;
			if ( ! empty( $this->params[ $index ]['alias'] ) ) {
				$alias = $this->params[ $index ]['alias'];
			}

			if ( $query ) {
				$item = "( {$item} )";
			}
			if ( ! $raw && ! $query ) {
				$item = $this->root()->escape( $item );
			}
			if ( $alias ) {
				$item .= " AS {$this->params[ $index ]['alias']}";
			}
			$sql .= "{$item}, ";
		}
		$sql = rtrim( $sql );
		$sql = rtrim( $sql, ',' );

		return $sql;
	}
}
