<?php


namespace ComposePress\Models\Query\Element;


use ComposePress\Models\Abstracts\QueryElement;

class OrderBy extends QueryElement {

	/**
	 * @return string
	 */
	public function __toString() {
		$sql = 'ORDER BY ';

		foreach ( $this->items as $index => $item ) {
			$order = null;
			$raw   = ! empty( $this->params[ $index ]['raw'] );

			if ( isset( $this->params[ $index ]['order'] ) ) {
				$order = strtoupper( $this->params[ $index ]['order'] );
			}
			if ( ! $raw ) {
				$item = $this->root()->escape( $item );
			}

			$sql .= "{$item} {$order}, ";
		}

		return trim( $sql, ', ' );
	}
}
