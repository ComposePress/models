<?php


namespace ComposePress\Models\Query\Element;


class Having extends Where {

	/**
	 * @return string
	 */
	public function __toString() {
		$sql = 'HAVING ';
		$sql .= $this->condition;

		return $sql;
	}
}
