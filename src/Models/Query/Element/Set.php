<?php


namespace ComposePress\Models\Query\Element;


use ComposePress\Models\Abstracts\QueryElement;

class Set extends QueryElement {

	/**
	 * @return string
	 */
	public function __toString() {
		$sql = 'SET ';
		foreach ( $this->items as $index => $item ) {
			$raw   = ! empty( $this->params[ $index ]['raw'] );
			$value = null;

			if ( isset( $this->params[ $index ]['value'] ) ) {
				$value = $this->params[ $index ]['value'];
			}

			$sql .= "{$this->root()->escape( $item )} = ";

			if ( ! $raw ) {
				$value = $this->wpdb->prepare( '%s', $value );
			}
			$sql .= "{$value}, ";
		}

		return trim( $sql, ', ' );
	}
}
