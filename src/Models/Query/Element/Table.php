<?php


namespace ComposePress\Models\Query\Element;


use ComposePress\Models\Abstracts\QueryElement;

class Table extends QueryElement {

	/**
	 * @return string
	 */
	public function __toString() {
		$sql = '';
		if ( 'SELECT' === $this->root()->get_mode() ) {
			$sql = 'FROM ';
		}

		foreach ( $this->items as $index => $item ) {
			$alias = null;
			if ( isset( $this->params[ $index ]['alias'] ) ) {
				$alias = $this->params[ $index ]['alias'];
			}

			$sql .= $item;
			if ( $alias ) {
				$sql .= " AS {$alias}";
			}
			$sql .= ', ';
		}

		return trim( $sql, ', ' );
	}
}
