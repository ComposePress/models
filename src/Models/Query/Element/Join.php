<?php


namespace ComposePress\Models\Query\Element;


class Join extends Condition {

	/**
	 * @return string
	 */
	public function __toString() {
		$sql = '';
		foreach ( $this->items as $index => $item ) {
			$condition = $this->params[ $index ]['condition'];
			$alias     = $this->params[ $index ]['alias'];
			$type      = $this->params[ $index ]['type'];

			$sql .= "{$type} JOIN {$item} AS {$alias} ON {$condition} ";
		}

		return trim( $sql );
	}
}
