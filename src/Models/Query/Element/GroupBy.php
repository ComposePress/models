<?php


namespace ComposePress\Models\Query\Element;


use ComposePress\Models\Abstracts\QueryElement;

class GroupBy extends QueryElement {

	/**
	 * @return string
	 */
	public function __toString() {
		$sql = 'GROUP BY ';

		foreach ( $this->items as $index => $item ) {
			$order = null;
			$raw   = ! empty( $this->params[ $index ]['raw'] );

			if ( ! $raw ) {
				$item = $this->root()->escape( $item );
			}

			$sql .= "{$item} {$order}, ";
		}

		return trim( $sql, ', ' );
	}
}
