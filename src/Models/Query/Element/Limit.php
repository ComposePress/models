<?php


namespace ComposePress\Models\Query\Element;


use ComposePress\Models\Abstracts\QueryElement;

/**
 * Class Limit
 *
 * @package ComposePress\Models\Query\Element
 * @property int $start
 * @property int $end
 */
class Limit extends QueryElement {
	/**
	 * @var int
	 */
	protected $start;

	/**
	 * @var int
	 */
	protected $end;

	/**
	 * @return string
	 */
	public function __toString() {
		$sql = "LIMIT {$this->end}";
		if ( ! empty( $this->start ) ) {
			$sql .= ", {$this->start}";
		}
		return $sql;
	}

	/**
	 * @return int
	 */
	public function get_start() {
		return $this->start;
	}

	/**
	 * @param int $start
	 */
	public function set_start( $start ) {
		$this->start = (int) $start;
	}

	/**
	 * @return int
	 */
	public function get_end() {
		return $this->end;
	}

	/**
	 * @param int $end
	 */
	public function set_end( $end ) {
		$this->end = (int) $end;
	}
}
