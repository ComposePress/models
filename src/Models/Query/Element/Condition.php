<?php


namespace ComposePress\Models\Query\Element;


use ComposePress\Models\Abstracts\QueryElement;

/**
 * Class Condition
 *
 * @package ComposePress\Models\Query\Element
 * @property string $type
 * @property \wpdb  $wpdb
 */
class Condition extends QueryElement {
	/**
	 * @var string
	 */
	protected $type = 'AND';

	/**
	 * @return string
	 */
	public function get_type() {
		return $this->type;
	}

	/**
	 * @param string $type
	 */
	public function set_type( $type ) {
		$this->type = $type;
	}

	/**
	 * @return string
	 */
	public function __toString() {
		$sql = '( ';
		foreach ( $this->items as $index => $item ) {
			$sql .= $this->process( $index, $item );
		}

		$sql = trim( $sql );
		$sql .= ' )';

		return $sql;
	}

	private function process( $index, $item ) {
		$comparison  = null;
		$value       = null;
		$raw         = null;
		$sql_part    = null;
		$array_value = null;
		$start_value = null;
		$end_value   = null;
		$auto_like   = true;
		$sql         = '';

		if ( $item instanceof self ) {
			$sql .= $item;

			if ( $index < count( $this->items ) - 1 ) {
				$sql .= " {$this->type} ";
			}

			return $sql;
		}

		if ( ! empty( $this->params[ $index ]['compare'] ) ) {
			$comparison = strtoupper( $this->params[ $index ]['compare'] );
		}

		if ( isset( $this->params[ $index ]['value'] ) ) {
			$value = $this->params[ $index ]['value'];
		}

		$raw_field    = ! empty( $this->params[ $index ]['raw_field'] );
		$raw_value    = ! empty( $this->params[ $index ]['raw_value'] );
		$column_value = ! empty( $this->params[ $index ]['column_value'] );

		if ( 'BETWEEN' === $comparison ) {
			$start_value = strtoupper( $this->params[ $index ]['start_value'] );
			$end_value   = strtoupper( $this->params[ $index ]['end_value'] );
		}

		if ( isset( $this->params[ $index ]['auto_like'] ) ) {
			$auto_like = (bool) $this->params[ $index ]['auto_like'];
		}

		if ( $raw_field ) {
			$sql .= "{$item} ";
		} else {
			$sql .= $this->root()->escape( $item ) . ' ';
		}

		$sql .= "{$comparison} ";

		if ( ! $array_value && 'BETWEEN' === $comparison ) {
			if ( $raw_value ) {
				$sql .= "{$start_value} AND {$end_value}";
			} else {
				$sql .= "{$this->wpdb->prepare( '%s', $start_value )} AND {$this->wpdb->prepare( '%s', $end_value )}";
			}
		} else {
			if ( is_array( $value ) ) {
				$array_value = '(';
				if ( $raw_value ) {
					$array_value .= implode( ', ', $value );
				} else {
					$array_value .= str_repeat( '%s, ', count( $value ) );
				}
				$array_value = rtrim( $array_value, ', ' );
				$array_value .= ')';

				if ( $raw_value ) {
					$sql .= $array_value;
				} else {
					$sql .= $this->wpdb->prepare( $array_value, $value );
				}
			}
		}


		if ( ! $array_value && 'LIKE' === $comparison && $auto_like ) {
			$value     = "%{$value}%";
			$raw_value = false;
		}

		if ( 'BETWEEN' !== $comparison ) {
			if ( ! $raw_value && ! $array_value ) {
				if ( $column_value ) {
					$sql .= $this->root()->escape( $item );
				} else {
					$sql .= $this->wpdb->prepare( '%s', $value );
				}
			}

			if ( $raw_value && ! $array_value ) {
				$sql .= $value;
			}

		}
		$sql .= ' ';

		if ( $index < count( $this->items ) - 1 ) {
			$sql .= " {$this->type} ";
		}

		return $sql;
	}

	/**
	 * @return mixed
	 * @throws \ComposePress\Core\Exception\Plugin
	 */
	public function new_condition() {
		return $this->create_component( __CLASS__ );
	}

	/**
	 * @param       $field
	 * @param       $comparison
	 * @param       $value
	 * @param bool  $raw_field
	 *
	 * @param bool  $raw_value
	 * @param array $extra_params
	 *
	 * @throws \ComposePress\Models\Exceptions\QueryException
	 */
	public function add_field_condition( $field, $comparison, $value, $raw_field = false, $raw_value = false, array $extra_params = [] ) {
		$params = array_merge( $extra_params, [
			'compare'   => $comparison,
			'value'     => $value,
			'raw_field' => $raw_field,
			'raw_value' => $raw_value,
		] );


		$this->add( $field, $params );
	}

	public function add_between_field_condition( $field, $start, $end, $raw_field = false, $raw_value = false, array $extra_params = [] ) {
		$params = array_merge( $extra_params, [
			'compare'     => 'BETWEEN',
			'start_value' => $start,
			'end_value'   => $end,
			'raw_field'   => $raw_field,
			'raw_value'   => $raw_value,
		] );


		$this->add( $field, $params );
	}

	public function add_join_condition( $field, $comparison, $value, array $extra_params = [] ) {
		$params = array_merge( $extra_params, [
			'compare'      => $comparison,
			'value'        => $value,
			'raw_field'    => false,
			'raw_value'    => false,
			'column_value' => true,
		] );


		$this->add( $field, $params );
	}


	public function add_raw_condition( $condition ) {
		$params = [ 'raw' => true ];

		$this->add( $condition, $params );
	}

	/**
	 * @param $field
	 *
	 * @throws \ComposePress\Models\Exceptions\QueryException
	 */
	public function add_condition( $field ) {
		$this->add( $field );
	}
}
