<?php


namespace ComposePress\Models\Query\Element;


use ComposePress\Models\Abstracts\QueryElement;

/**
 * Class Where
 *
 * @package ComposePress\Models\Query\Element
 * @property \ComposePress\Models\Query\Element\Condition $condition
 */
class Where extends QueryElement {
	/**
	 * @var \ComposePress\Models\Query\Element\Condition
	 */
	protected $condition;

	/**
	 * @return string
	 */
	public function __toString() {
		$sql = 'WHERE ';
		$sql .= $this->condition;

		return $sql;
	}

	/**
	 * @return string
	 */
	public function get_condition() {
		return $this->condition;
	}

	/**
	 * @param string $condition
	 */
	public function set_condition( Condition $condition ) {
		$this->condition = $condition;
	}
}
