<?php


namespace ComposePress\Models;


use ComposePress\Core\Abstracts\Component_0_7_5_0;

/**
 * Class Table
 *
 * @package ComposePress\Models
 * @property \ComposePress\Models\Abstracts\Model $parent
 * @property \wpdb                                $wpdb
 * @property string                               $name
 * @property string                               $full_name
 */
class Table extends Component_0_7_5_0 {


	/**
	 * @return bool
	 */
	public function setup() {
		if ( $this->parent->drop_on_uninstall ) {
			add_action( 'uninstall_' . plugin_basename( $this->plugin->plugin_file ), [ $this, 'uninstall' ], 11 );
		}
		add_action( 'activate_' . plugin_basename( $this->plugin->plugin_file ), [ $this, 'build' ], 11 );

		if ( is_multisite() && ! $this->parent->use_multisite_global_table ) {
			add_action( 'wpmu_new_blog', [ $this, 'build_new_blog' ] );
			add_filter( 'wpmu_drop_tables', [ $this, 'update_mu_tables' ], 10, 2 );
		}

		return true;
	}

	/**
	 * @param $network_wide
	 */
	public function build( $network_wide ) {
		require_once ABSPATH . 'wp-admin/includes/upgrade.php';

		$single_site      = $this->parent->singlesite_enabled;
		$multisite        = $this->parent->multisite_enabled;
		$multisite_global = $this->parent->use_multisite_global_table;
		$parent           = $this->parent;

		if ( $single_site ) {
			dbDelta( static::build_schema( $parent::MODE_SINGLESITE ) );
		}
		if ( $network_wide && $multisite && ! $multisite_global ) {
			$sites = get_sites( [ 'fields' => 'ids' ] );

			if ( 0 < count( $sites ) ) {
				foreach ( $sites as $site ) {
					switch_to_blog( $site );
					dbDelta( static::build_schema( $parent::MODE_MULTISITE ) );
					restore_current_blog();
				}
			}
		}
		if ( $network_wide && $multisite && $multisite_global ) {
			dbDelta( static::build_schema( $parent::MODE_MULTISITE_GLOBAL ) );
		}
	}

	/**
	 * @param $mode
	 *
	 * @return string
	 */
	protected function build_schema( $mode ) {
		$schema_data = $this->parent->get_schema( $mode );

		$original_mode            = $this->parent->query_mode;
		$this->parent->query_mode = $mode;

		$schema_data['name'] = $this->get_full_name();

		$schema_builder = $this->get_schema_builder( $schema_data );

		$sql = $schema_builder->get_sql();

		$this->parent->query_mode = $original_mode;

		return $sql;
	}

	/**
	 * @return string
	 */
	public function get_prefix() {
		$parent = $this->parent;
		$prefix = '';
		if ( is_multisite() && $parent::MODE_MULTISITE_GLOBAL === $this->query_mode ) {
			$prefix = $this->wpdb->base_prefix;
		}
		if ( ! is_multisite() || ( is_multisite() && $parent::MODE_MULTISITE === $this->query_mode ) ) {
			$prefix = $this->wpdb->prefix;
		}

		return $prefix;
	}

	/**
	 * @return string
	 */
	public function get_full_name() {
		$parent = $this->parent;
		$schema = $this->get_prefix();

		$schema .= static::get_name();

		return $schema;
	}

	/**
	 * @return string
	 */
	public function get_name() {
		$parent = $this->parent;

		return strtolower( $parent::NAME );
	}

	public function get_schema_builder( $schema = [] ) {
		/** @var \ComposePress\Models\SchemaBuilder $schema_builder */
		$schema_builder = $this->create_component( '\ComposePress\Models\SchemaBuilder' );
		$schema_builder->set_params( $schema );
		$schema_builder->init();

		return $schema_builder;
	}

	/**
	 * @param $blog_id
	 */
	public function build_new_blog( $blog_id ) {
		$parent = $this->parent;
		switch_to_blog( $blog_id );
		dbDelta( static::build_schema( $parent::MODE_MULTISITE ) );
		restore_current_blog();
	}

	/**
	 * @param $tables
	 * @param $blog_id
	 *
	 * @return array
	 */
	public function update_mu_tables( $tables, $blog_id ) {
		$tables[] = $this->wpdb->get_blog_prefix( $blog_id ) . $this->parent->get_name();

		return $tables;
	}

	public function uninstall() {
		$this->wpdb->query( "DROP TABLE IF EXISTS {$this->parent->get_full_name()}" );
	}

}
