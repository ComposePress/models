<?php


namespace ComposePress\Models\Abstracts;


/**
 * Class QueryStatement
 *
 * @package ComposePress\Models\Abstracts
 * @property string $type
 */
abstract class QueryStatement extends QueryElement {
	/**
	 * @var string
	 */
	protected $type;

	/**
	 * @return string
	 */
	public function get_type() {
		return $this->type;
	}
}
