<?php


namespace ComposePress\Models\Abstracts;


use ComposePress\Core\Abstracts\Component_0_7_5_0;
use ComposePress\Models\Exceptions\InvalidSchema;

/**
 * Class Element
 *
 * @package ComposePress\Models\Schema
 */
abstract class SchemaElement extends Component_0_7_5_0 {

	/**
	 * @return string|null
	 */
	abstract public function get_sql();

	/**
	 * @param $params
	 *
	 * @throws \ComposePress\Models\Exceptions\InvalidSchema
	 */
	public function set_params( $params ) {
		foreach ( $params as $param => $value ) {
			$param = strtolower( $param );
			if ( method_exists( $this, "set_{$param}" ) ) {
				$this->{"set_{$param}"}( $value );
				continue;
			}
			if ( property_exists( $this, $param ) ) {
				$this->{$param} = $value;
				continue;
			}
			throw new InvalidSchema( sprintf( '%s parameter does not exist for Schema Element %s', $param, $this->get_full_class_name() ) );
		}
	}

	/**
	 * @param string $filter_out,...
	 *
	 * @return array
	 */
	public function get_params() {
		$items = get_object_vars( $this );

		return array_diff_key( $items, array_flip( func_get_args() ) );
	}
}
