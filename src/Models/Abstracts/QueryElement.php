<?php


namespace ComposePress\Models\Abstracts;


use ComposePress\Core\Abstracts\Component_0_7_5_0;
use ComposePress\Models\Exceptions\QueryException;
use Iterator;

/**
 * Class QueryElement
 *
 * @package ComposePress\Models\Abstracts
 * @property \ComposePress\Models\Abstracts\QueryElement|\ComposePress\Models\Abstracts\Model $parent
 */
abstract class QueryElement extends Component_0_7_5_0 implements Iterator {

	/**
	 * @var \ComposePress\Models\Abstracts\QueryElement[]|array
	 */
	protected $items = [];

	/**
	 * @var array
	 */
	protected $params = [];

	/**
	 * @param $item
	 *
	 * @throws \ComposePress\Models\Exceptions\QueryException
	 */
	public function add( $item, $params = [] ) {

		if ( is_bool( $item ) ) {
			throw new QueryException( sprintf( 'Item cannot be boolean in element %s', $this->get_full_class_name() ) );
		}

		if ( is_array( $item ) ) {
			foreach ( $item as $index => $the_item ) {
				$this->add( $the_item, isset( $params[ $index ] ) ? isset( $params[ $index ] ) : [] );
			}

			return $this;
		}
		$this->items[]  = $item;
		$this->params[] = $params;

		return $this;
	}

	/**
	 *
	 */
	public function __destruct() {
		/** @var \ComposePress\Models\Abstracts\QueryElement $item */
		foreach ( $this as $item ) {
			if ( is_array( $item ) || ! is_object( $item ) ) {
				continue;
			}
			$item->__destruct();
		}
		parent::__destruct();
	}

	/**
	 * @return array
	 */
	public function get_items() {
		return $this->items;
	}

	/**
	 * @return string
	 */
	public abstract function __toString();

	/**
	 * @return mixed
	 */
	public function current() {
		return current( $this->items );
	}

	/**
	 * @return void
	 */
	public function next() {
		next( $this->items );
	}

	/**
	 * @return int|mixed|string|null
	 */
	public function key() {
		return key( $this->items );
	}

	/**
	 * @return bool
	 */
	public function valid() {
		return null !== key( $this->items );
	}

	/**
	 * @return void
	 */
	public function rewind() {
		reset( $this->items );
	}

	/**
	 * @return \ComposePress\Models\Query\Builder
	 */
	public function root() {
		$parent = $this;
		/** @var \ComposePress\Models\Query\Builder $parent_parent */
		while ( $parent_parent = $parent->get_closest( '\ComposePress\Models\Query\Builder' ) ) {
			$parent = $parent_parent;
		}

		return $parent;
	}
}
