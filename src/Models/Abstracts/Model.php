<?php

namespace ComposePress\Models\Abstracts;

use ComposePress\Core\Abstracts\Component_0_7_5_0;
use ComposePress\Models\Exceptions\ModelException;
use ComposePress\Models\SchemaBuilder;
use ComposePress\Models\Table;

/**
 * Class Model
 *
 * @package ComposePress\Models\Abstracts
 * @method int|false insert( array $data, array | string $format = null )
 * @method int|false update( array $data, array $where, array | string $format = null, array | string $where_format = null )
 * @method int|false replace( array $data, array | string $format = null )
 * @method int|false delete( array $where, array | string $format = null )
 * @method int|false get_row( array $data, array | string $format = null )
 * @method int|false insert_multisite( array $data, array | string $format = null )
 * @method int|false update_multisite( array $data, array $where, array | string $format = null, array | string $where_format = null )
 * @method int|false replace_multisite( array $data, array | string $format = null )
 * @method int|false delete_multisite( array $where, array | string $format = null )
 * @property \wpdb         $wpdb
 * @property string        $query_mode
 * @property SchemaBuilder $schema_builder
 * @property bool          $drop_on_uninstall
 * @property bool          $use_multisite_global_table
 * @property bool          $singlesite_enabled
 * @property bool          $multisite_enabled
 * @property Table         $table
 */
abstract class Model extends Component_0_7_5_0 {

	/**
	 * Table name
	 */
	const NAME = '';

	/**
	 * Query and build mode for a default single wordpress site
	 */
	const MODE_SINGLESITE = 'single';

	/**
	 *  Query and build mode for multisite but make a table for each subsite
	 */
	const MODE_MULTISITE = 'multiple';

	/**
	 *  Query and build mode for multisite but make a table globally for all sites
	 */
	const MODE_MULTISITE_GLOBAL = 'single_global';


	/**
	 *
	 */
	const ENTITY_MODEL_CLASS = '\ComposePress\Models\EntityModel';

	/**
	 *
	 */
	const ENTITY_COLLECTION_CLASS = '\ComposePress\Models\EntityCollection';

	/**
	 *
	 */
	const TABLE_CLASS = '\ComposePress\Models\Table';


	/**
	 *
	 */
	const DROP_ON_UNINSTALL = false;

	/**
	 * Current sql mode
	 *
	 * @var string
	 */
	protected $query_mode;

	/**
	 * @var \ComposePress\Models\Table
	 */
	protected $table;

	/**
	 * Model constructor.
	 *
	 * @param \ComposePress\Models\SchemaBuilder $schema_builder
	 */
	public function __construct( Table $table ) {
		$this->table = $table;
	}


	/**
	 * @return bool
	 */
	public function setup() {
		$this->query_mode = self::MODE_SINGLESITE;

		return true;
	}

	/**
	 * @return bool
	 */
	public function get_drop_on_uninstall() {
		return static::DROP_ON_UNINSTALL;
	}

	/**
	 * @return bool
	 */
	public function get_singlesite_enabled() {
		return true;
	}

	/**
	 * @param $query
	 *
	 * @return bool|\ComposePress\Models\EntityCollection
	 * @throws \ComposePress\Models\Exceptions\InvalidData
	 * @throws \ComposePress\Core\Exception\Plugin
	 * @throws \ComposePress\Core\Exception\ComponentInitFailure
	 * @throws \ReflectionException
	 */
	public function sql( $query ) {

		$query = str_replace( '{table}', $this->table->get_full_name(), $query );

		$result = $this->wpdb->get_results( $query, ARRAY_A );

		if ( empty( $result ) ) {
			return false;
		}

		/** @var \ComposePress\Models\EntityCollection $collection */
		$collection = $this->get_new_collection()->add_many( $result );
		foreach ( $collection as $entity ) {
			$entity->set_is_new( false );
			$entity->set_is_dirty( false );
		}

		return $collection;
	}

	/**
	 * @return \ComposePress\Models\Query\Builder
	 * @throws \ComposePress\Core\Exception\ComponentInitFailure
	 * @throws \ComposePress\Core\Exception\Plugin
	 * @throws \ReflectionException
	 */
	public function query() {
		/** @var \ComposePress\Models\Query\Builder $builder */
		$builder = $this->create_component( '\ComposePress\Models\Query\Builder' );
		$builder->init();

		return $builder;
	}

	/**
	 * @return \ComposePress\Models\EntityCollection
	 * @throws \ComposePress\Core\Exception\Plugin
	 * @throws \ComposePress\Core\Exception\ComponentInitFailure
	 * @throws \ReflectionException
	 */
	public function get_new_collection() {
		$class = static::ENTITY_COLLECTION_CLASS;
		/** @var \ComposePress\Models\EntityCollection $instance */
		$instance = $this->create_component( $class );
		$instance->init();

		return $instance;
	}

	/**
	 * @return \ComposePress\Models\EntityModel
	 * @throws \ComposePress\Core\Exception\Plugin
	 * @throws \ComposePress\Core\Exception\ComponentInitFailure
	 * @throws \ReflectionException
	 */
	public function get_new() {
		$class = static::ENTITY_MODEL_CLASS;
		/** @var \ComposePress\Models\EntityModel $instance */
		$instance = $this->create_component( $class );
		$instance->init();

		return $instance;
	}

	/**
	 * @param $name
	 * @param $arguments
	 *
	 * @return mixed
	 * @throws \ComposePress\Models\Exceptions\ModelException
	 */
	public function __call( $name, $arguments ) {

		$multisite_methods = [
			'insert_multisite',
			'update_multisite',
			'replace_multisite',
			'delete_multisite',
		];


		if ( ! is_multisite() && in_array( $name, $multisite_methods ) ) {
			throw new ModelException( sprintf( 'Can not call %s on %s when not in multisite mode', $name, $this->get_full_class_name() ) );
		}

		if ( in_array( $name, array_merge( [
			'insert',
			'update',
			'replace',
			'delete',
			$multisite_methods,
		] ) ) ) {
			array_unshift( $arguments, $this->table->get_full_name() );

			return call_user_func_array( [ $this->wpdb, $name ], $arguments );
		}

		throw new ModelException( sprintf( 'Method %s does not exist on %s', $name, $this->get_full_class_name() ) );
	}

	/**
	 * @param $id
	 *
	 * @return bool|\ComposePress\Models\EntityModel
	 * @throws \ComposePress\Models\Exceptions\InvalidData
	 * @throws \ComposePress\Core\Exception\Plugin
	 * @throws \ComposePress\Core\Exception\ComponentInitFailure
	 * @throws \ReflectionException
	 */
	public function find( $id ) {
		$schema = $this->get_schema( $this->query_mode );

		$primary_key = $schema['primary_key'];

		$select = sprintf( 'SELECT * FROM %s ', $this->table->get_full_name() );
		$where  = $this->wpdb->prepare( "WHERE `{$primary_key}` = %d", $id );

		$result = $this->wpdb->get_row( sprintf( ' %s %s', $select, $where ), ARRAY_A );

		if ( empty( $result ) ) {
			return false;
		}

		return $this->get_new_entity_from_data( $result );
	}

	/**
	 * @param $mode
	 *
	 * @return mixed
	 */
	abstract public function get_schema( $mode );

	/**
	 * @param $data
	 *
	 * @return \ComposePress\Models\EntityModel
	 * @throws \ComposePress\Models\Exceptions\InvalidData
	 * @throws \ComposePress\Core\Exception\Plugin
	 * @throws \ComposePress\Core\Exception\ComponentInitFailure
	 * @throws \ReflectionException
	 */
	protected function get_new_entity_from_data( $data ) {
		$entity = $this->get_new();

		$entity->set_data( $data );
		$entity->set_is_new( false );
		$entity->set_is_dirty( false );

		return $entity;
	}

	/**
	 * @return string
	 */
	public function get_query_mode() {
		return $this->query_mode;
	}

	/**
	 * @param string $query_mode
	 *
	 * @return bool
	 * @throws \ComposePress\Models\Exceptions\ModelException
	 */
	public function set_query_mode( $query_mode ) {
		if ( self::MODE_MULTISITE_GLOBAL === $query_mode && ! static::get_multisite_enabled() && ! static::get_use_multisite_global_table() ) {
			return false;
		}
		if ( self::MODE_MULTISITE === $query_mode && ! static::get_multisite_enabled() ) {
			return false;
		}

		if ( ! in_array( $query_mode, [ self::MODE_SINGLESITE, self::MODE_MULTISITE, self::MODE_MULTISITE_GLOBAL ] ) ) {
			throw new  ModelException( sprintf( 'Invalid sql mode %s on model %s', $query_mode, $this->get_full_class_name() ) );
		}

		$this->query_mode = $query_mode;

		return true;
	}

	/**
	 * @return bool
	 */
	public function get_multisite_enabled() {
		return false;
	}

	/**
	 * @return bool
	 */
	public function get_use_multisite_global_table() {
		return false;
	}

	/**
	 * @return \ComposePress\Models\Table
	 */
	public function get_table() {
		return $this->table;
	}

}
